# Sibujang Android Application for RUMA
[![build status](https://gitlab.com/PPL2017csui/PPLD7/badges/master/build.svg)](https://gitlab.com/PPL2017csui/PPLD7/commits/master)
[![coverage report](https://gitlab.com/PPL2017csui/PPLD7/badges/master/coverage.svg)](https://codecov.io/gl/PPL2017csui/PPLD7/branch/master)
# Sibujang App - Aplikasi Sistem Pendistribusian Kinerja Tim Lapangan


Sibujang app adalah aplikasi yang akan memberikan sales force PT RUMA informasi mengenai pencapaian yang telah mereka raih.

# Contributors
- Mila Alief Alya
- Muhammad Sabiq Danurrohman
- Mutia Khairunnisa
- Norma Puspitasari
- Rizky Noviandi Purwono
