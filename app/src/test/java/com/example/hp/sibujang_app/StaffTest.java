package com.example.hp.sibujang_app;

import org.junit.*;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
/**
 * Created by Sabiq.
 */
@Config(constants = BuildConfig.class, packageName = "com.example.hp.sibujang_app")
@RunWith(RobolectricTestRunner.class)

public class StaffTest {
    private Staff staff;

    private String id = "58dab7b886450c413d083365";
    private String code = "000-376";
    private String name = "PY 376";
    private String branchCode = "93";
    private String branchName = "KARAWANG";
    private String BMFOCode = "BMFO-73";

    @Before
    public void setup() {
        staff = new Staff(id, code, name, branchCode, branchName, BMFOCode);
    }

    @Test
    public void codeCheck() {
        Staff staff = Mockito.mock(Staff.class);
        when(staff.getCode()).thenReturn("000-376");
    }

    @Test
    public void nameCheck() {
        Staff staff = Mockito.mock(Staff.class);
        when(staff.getName()).thenReturn("PY 376");
    }

//    @Test
//    public void branchCodeCheck() {
//        Staff staff = Mockito.mock(Staff.class);
//        when(staff.getBranchCode()).thenReturn("93");
//    }
//
//    @Test
//    public void branchNameCheck() {
//        Staff staff = Mockito.mock(Staff.class);
//        when(staff.getBranchName()).thenReturn("KARAWANG");
//    }
//
//    @Test
//    public void BMFOCodeCheck() {
//        Staff staff = Mockito.mock(Staff.class);
//        when(staff.getBMFOCode()).thenReturn("BMFO-73");
//    }
}
