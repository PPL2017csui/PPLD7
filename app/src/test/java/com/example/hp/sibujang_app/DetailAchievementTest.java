package com.example.hp.sibujang_app;

import android.content.Intent;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.util.ArrayList;

import static junit.framework.Assert.assertNotNull;

/**
 * Created by Sabiq on 27/04/2017.
 */
@Config(constants = BuildConfig.class, packageName = "com.example.hp.sibujang_app")
@RunWith(RobolectricTestRunner.class)
public class DetailAchievementTest {
    private DetailAchievement activity;
    private ArrayList detailAchievement;
    private String achievementTitle;

    @Before
    public void setup() throws Exception {
        achievementTitle = "List Staff\\(Tgl)";
        detailAchievement = new ArrayList<String>();
        detailAchievement.add("id:58da61d986450c0984efb065");
        detailAchievement.add("kodeStaff:000-1");
        detailAchievement.add("jenis:Kunjungan");
        detailAchievement.add("periode:Mingguan");
        detailAchievement.add("tanggal:2017-01-01T00:00:00.000Z");
        detailAchievement.add("Jumlah Rencana Kunjungan:13");
        detailAchievement.add("Jumlah Kunjungan:6");
        detailAchievement.add("Jumlah Follow Up:5");
        detailAchievement.add("Jumlah Kunjungan Gagal:7");
        Intent intent = new Intent();
        intent.putExtra("achievementTitle", achievementTitle);
        intent.putStringArrayListExtra("achievementAttribute",detailAchievement);
        intent.putExtra("achievementType", "KPI");
        activity = Robolectric.buildActivity(DetailAchievement.class).withIntent(intent).create().get();
        assertNotNull(activity);
    }

    @Test
    public void checkActivityNotNull() throws Exception {
        assertNotNull("How come MainActivity is null???", activity);
    }
}
