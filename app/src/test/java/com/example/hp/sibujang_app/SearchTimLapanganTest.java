package com.example.hp.sibujang_app;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;

import com.example.hp.sibujang_app.notification.RegistrationService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

/**
 * Created by Ismatullah on 18/05/2017.
 */

@Config(constants = BuildConfig.class, packageName = "com.example.hp.sibujang_app")
@RunWith(RobolectricTestRunner.class)
public class SearchTimLapanganTest {

    private SearchTimLapangan search;
    private SearchTimLapangan.SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;

    @Before
    public void setup() throws Exception {
        search = Robolectric.buildActivity(SearchTimLapangan.class).get();
    }

    @Test
    public void checkActivityNotNull() throws Exception {
        assertNotNull(search);
    }

    @Test
    public void checkOnCreate() throws Exception {

    }

    @Test
    public void checkPlaceholder() throws Exception {
        assertNotNull(search.getApplicationContext());
    }
}
