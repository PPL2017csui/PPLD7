package com.example.hp.sibujang_app;

import android.content.Intent;
import android.widget.FrameLayout;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;

/**
 * Created by Sabiq on 04/05/2017.
 */

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class,packageName = "com.example.hp.sibujang_app")
public class SearchResultAdapterTest {
    SearchResultActivity activity;
    SearchResultAdapter adapter;
    String namaPencapaian;
    String tanggalPencapaian;
    private List<Achievement> my_data;
    //ArrayList<String> data;

    @Before
    public void setup() throws Exception {
        Intent intent = new Intent();
        intent.putExtra("namaPencapaian","KPI");
        intent.putExtra("tanggalPencapaian","2017-1-1");
        activity = Robolectric.buildActivity(SearchResultActivity.class).withIntent(intent).create().get();
        String type = "kunjungan";
        String date = "2017-01-01";
        String attribute = "Target";
        String value = "50";
        ArrayList<String> attributes= new ArrayList<>();
        attributes.add(attribute + ": " + value);
        my_data = new ArrayList<Achievement>();
        my_data.add(new Achievement(type, date, attributes));
        adapter = new SearchResultAdapter(activity.getApplicationContext(), my_data, namaPencapaian, tanggalPencapaian);
    }

    @Test
    public void checkActivityNotNull() throws Exception {
        assertNotNull("How come MainActivity is null???", activity);
    }

    @Test
    public void getItemCount() throws Exception
    {
        assertTrue("Item List contains incorrect count",
                my_data.size() == adapter.getItemCount());
    }

    @Test
    public void onCreateViewHolder() throws Exception
    {
        SearchResultAdapter.ViewHolder viewHolder = adapter.onCreateViewHolder(
                new FrameLayout(RuntimeEnvironment.application), 0);
        assertNotNull(viewHolder);
    }

    @Test
    public void onBindViewHolder() throws Exception
    {
        SearchResultAdapter.ViewHolder viewHolder = adapter.onCreateViewHolder(
                new FrameLayout(RuntimeEnvironment.application), 0);
        adapter.onBindViewHolder(viewHolder, 0);
    }

    @Test
    public void performClick() throws Exception
    {
        SearchResultAdapter.ViewHolder viewHolder = adapter.onCreateViewHolder(
                new FrameLayout(RuntimeEnvironment.application), 0);
    }
}
