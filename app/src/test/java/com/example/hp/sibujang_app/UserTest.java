package com.example.hp.sibujang_app;

/**
 * Created by USER on 4/27/2017.
 */

import org.junit.*;
import org.mockito.Mockito;

import static org.mockito.Mockito.when;

public class UserTest {
    private User users;

    private String name = "PY 367";
    private String password = "PY367";
    private String kodeStaff = "000-367";
    private String token = "qkejfqhwidhaihfwaf";

    @Before
    public void setup(){
        users = new User(name,password,kodeStaff);
        users.setToken(token);
    }

    @Test
    public void nameCheck(){
        User user = Mockito.mock(User.class);
        when(user.getName()).thenReturn("PY 367");
    }

    @Test
    public void passwordCheck(){
        User user = Mockito.mock(User.class);
        when(user.getName()).thenReturn("PY367");
    }

    @Test
    public void positionCheck(){
        User user = Mockito.mock(User.class);
        when(user.getName()).thenReturn("Penyuluh");
    }

    @Test
    public void kodeStaffCheck(){
        User user = Mockito.mock(User.class);
        when(user.getKodeStaff()).thenReturn("000-367");
    }

    @Test
    public void tokenCheck(){
        User user = Mockito.mock(User.class);
        when(user.getName()).thenReturn("qkejfqhwidhaihfwaf");
    }

}
