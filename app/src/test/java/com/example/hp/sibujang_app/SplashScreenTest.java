package com.example.hp.sibujang_app;

import org.junit.*;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Sabiq on 26/04/2017.
 */
@Config(constants = BuildConfig.class, packageName = "com.example.hp.sibujang_app")
@RunWith(RobolectricTestRunner.class)
public class SplashScreenTest {
    private SplashScreen splashScreen;

    @Before
    public void setup() throws Exception {
        splashScreen = Robolectric.setupActivity(SplashScreen.class);
    }

    @Test
    public void checkActivityNotNull() throws Exception {
        assertNotNull("How come MainActivity is null???", splashScreen);
    }

}
