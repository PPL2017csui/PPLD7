package com.example.hp.sibujang_app;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.example.hp.sibujang_app.notification.RegistrationService;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;
import org.robolectric.shadow.api.Shadow;
import org.robolectric.shadows.ShadowEnvironment;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import static com.example.hp.sibujang_app.notification.RegistrationService.REGISTRATION_SUCCESS;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

/**
 * Created by Ismatullah on 20/04/2017.
 */
@Config(constants = BuildConfig.class, packageName = "com.example.hp.sibujang_app")
@RunWith(RobolectricTestRunner.class)
public class RegistrationServiceTest {
    private RegistrationService reg;

    @Before
    public void setup() throws Exception {
        reg = Robolectric.setupService(RegistrationService.class);
    }

    @Test
    public void checkIntentServiceNotNull() throws Exception {
        assertNotNull("How come RegistrationService is null???", reg);
    }

    @Test
    public void checkVariable() throws Exception {
        assertNotNull(reg.getApplicationContext());
        assertEquals("GCMTOKEN", reg.TAG);
        assertEquals("RegistrationSuccess", REGISTRATION_SUCCESS);
        assertEquals("RegistrationError", reg.REGISTRATION_ERROR);
    }

    @Test
    public void testSaveToServer() {
        String token = "abcd";
        Map paramPost = new HashMap();
        paramPost.put("registrationId", token);
        assertNotNull(paramPost.get("registrationId"));
        assertNotNull(reg.getApplicationContext());
    }

    @Test
    public void testGetStringResultPOST() throws IOException{
        URL url = new URL("https://sibujang-app-staging.herokuapp.com/devices");
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        assertEquals("GET", urlConnection.getRequestMethod());
        assertEquals(false, urlConnection.getDoOutput());
        assertEquals(true, urlConnection.getUseCaches());
        assertEquals(200, urlConnection.getResponseCode());
        Map paramPost = new HashMap();
        assertEquals(null, reg.getStringResultFromService_POST("https://sibujang-app-staging.herokuapp.com/devices", paramPost));
        assertNotNull(reg.getApplicationContext());
    }

    @Test(expected=MalformedURLException.class)
    public void testWrongUrl() throws IOException{
        URL wrongUrl = new URL("localhost/devices");
    }

    @Test
    public void registerGCMTest() throws IOException {
        SharedPreferences sharedPreferences = reg.getSharedPreferences("GCM", Context.MODE_PRIVATE); //Define shared reference file name
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Intent registrationComplete = null;
        String token = "abcd";
        InstanceID instanceID = InstanceID.getInstance(reg.getApplicationContext());
        Log.w("RegService", "token: " + token);
        // notify to UI registration complete success
        registrationComplete = new Intent(REGISTRATION_SUCCESS);
        registrationComplete.putExtra("token", token);
        String oldToken = sharedPreferences.getString("GCMTOKEN", ""); // Return "" when error or key not exists
        // Only request to save token when token is new
        if (!"".equals(token) && !oldToken.equals(token)) {
            // Sabe new token to shared reference
            editor.putString("GCMTOKEN", token);
            editor.commit();
        } else {
            Log.w("RegistrationService", "Old Token");
        }
        assertNotNull(reg.getApplicationContext());
    }
}
