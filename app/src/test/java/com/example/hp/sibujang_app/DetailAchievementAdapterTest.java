package com.example.hp.sibujang_app;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.util.ArrayList;

import static org.junit.Assert.assertNotNull;

/**
 * Created by Sabiq on 27/04/2017.
 */
@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class,packageName = "com.example.hp.sibujang_app")
public class DetailAchievementAdapterTest {
    private ArrayList<String> data;
    private Activity activity;
    private DetailAchievementAdapter profileAdapter;

    @Before
    public void setUp() throws Exception
    {
        data = new ArrayList<String>();
        data.add("id:58da61d986450c0984efb065");
        data.add("kodeStaff:000-1");
        data.add("jenis:Kunjungan");
        data.add("periode:Mingguan");
        data.add("tanggal:2017-01-01T00:00:00.000Z");
        data.add("Jumlah Rencana Kunjungan:13");
        data.add("Jumlah Kunjungan:6");
        data.add("Jumlah Follow Up:5");
        data.add("Jumlah Kunjungan Gagal:7");

      //  activity = Robolectric.setupActivity(MainActivity.class);
      //  profileAdapter = new DetailAchievementAdapter(activity, data);
    }

    @Test
    public void getViewWhenNull() throws Exception
    {
//        assertNotNull(profileAdapter.getView(0, null, null));
//
//        LayoutInflater inflater = activity.getLayoutInflater();
//        View view = inflater.inflate(R.layout.profile_content, null);
//
//        assertNotNull(view);
//        assertNotNull(view.findViewById(R.id.text1));
//        assertNotNull(view.findViewById(R.id.text2));

    }

}
