package com.example.hp.sibujang_app;

import android.content.Intent;

import com.example.hp.sibujang_app.notification.RegistrationService;
import com.example.hp.sibujang_app.notification.TokenRefreshListenerService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

/**
 * Created by Ismatullah on 27/04/2017.
 */
@Config(constants = BuildConfig.class, packageName = "com.example.hp.sibujang_app")
@RunWith(RobolectricTestRunner.class)
public class TokenListenerTest {
    private TokenRefreshListenerService tokenListener;

    @Before
    public void setup() throws Exception {
        tokenListener = Robolectric.setupService(TokenRefreshListenerService.class);
    }

    @Test
    public void checkIntentServiceNotNull() throws Exception {
        assertNotNull("How come Token Service is null???", tokenListener);
    }

    @Test
    public void checkIntent() throws Exception {
        tokenListener.onTokenRefresh();
        assertNotNull(tokenListener.getApplicationContext());
    }
}
