package com.example.hp.sibujang_app;

import android.content.Intent;
import android.support.design.widget.TextInputLayout;
import android.widget.Button;
import android.widget.TextView;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.Shadows;
import org.robolectric.android.controller.ActivityController;
import org.robolectric.annotation.Config;
import org.robolectric.manifest.AndroidManifest;
import org.robolectric.res.Fs;

import java.util.ArrayList;

import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.*;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;
/**
 * Class Representation for Login Unit Test
 */
@Config(constants = BuildConfig.class, packageName = "com.example.hp.sibujang_app")
@RunWith(RobolectricTestRunner.class)
public class LoginActivityTest {
    private LoginActivity activity;
    private LoginActivity.PostDataTask task;
    private User user;
    private User manager;

    @Before
    public void setup() throws Exception {
        activity = Robolectric.setupActivity(LoginActivity.class);
        user = new User("000-368", "PY368", "000-368");
        manager = new User("BMFO 1", "BMFO1", "BMFO-1");
//        user.setPosition();
//        task = new LoginActivity.PostDataTask(activity, user);*/
    }

    @Test
    public void checkActivityNotNull() throws Exception {
        assertNotNull("How come LoginActivity is null???", activity);
    }

    @Test
    public void userTest() throws Exception{
        assertNotNull(user.getName());
        assertEquals("000-368", user.getName());
        assertNotNull(user.getKodeStaff());
        assertEquals("000-368", user.getKodeStaff());
        assertNotNull(user.getPassword());
        assertEquals("PY368", user.getPassword());
        assertNotNull(user.getPosition());
    }

    @Test
    public void managerTest() throws Exception{
        assertNotNull(manager.getName());
        assertEquals("BMFO 1", manager.getName());
        assertNotNull(manager.getKodeStaff());
        assertEquals("BMFO-1", manager.getKodeStaff());
        assertNotNull(manager.getPassword());
        assertEquals("BMFO1", manager.getPassword());
        assertNotNull(manager.getPosition());
        manager.getToken();
    }

  /*  @Test
    public void buttonClickShouldStartNewActivity() throws Exception
    {
        Button button = (Button) activity.findViewById(R.id.bLogin);
        button.performClick();
        Intent intent = Shadows.shadowOf(activity).peekNextStartedActivity();
        assertEquals(task.execute(Constants.BASE_URL), intent.getComponent().getClassName());
    }*/

}
