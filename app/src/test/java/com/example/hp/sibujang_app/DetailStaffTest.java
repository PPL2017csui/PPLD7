package com.example.hp.sibujang_app;

        import android.content.Intent;
        import android.widget.ListView;

        import org.junit.Before;
        import org.junit.Test;
        import org.junit.runner.RunWith;
        import org.robolectric.Robolectric;
        import org.robolectric.RobolectricTestRunner;
        import org.robolectric.annotation.Config;

        import java.util.ArrayList;

        import static junit.framework.Assert.assertNotNull;

/**
 * Created by Mutia on 5/18/17.
 */

@Config(constants = BuildConfig.class, packageName = "com.example.hp.sibujang_app")
@RunWith(RobolectricTestRunner.class)
public class DetailStaffTest {
    private DetailStaff activity;
    private ArrayList<String> data;

    @Before
    public void setup() throws Exception {
        data = new ArrayList<String>();
        data.add("Kode Staf:000-375");
        data.add("Branch:KARAWANG");
        data.add("Nama BMFO:BMFO-73");
        Intent intent = new Intent();
        intent.putExtra("staffName","PY 375");
        intent.putStringArrayListExtra("staffData",data);
        activity = Robolectric.buildActivity(DetailStaff.class).withIntent(intent).create().get();
        assertNotNull(activity);
    }

    @Test
    public void checkActivityNotNull() throws Exception {
        assertNotNull("How come MainActivity is null???", activity);
    }
}