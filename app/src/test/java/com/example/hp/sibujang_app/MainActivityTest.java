package com.example.hp.sibujang_app;


import android.content.IntentFilter;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;

import com.example.hp.sibujang_app.notification.RegistrationService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

/**
 * Class Representation for Unit Test MainActivity
 */

@Config(constants = BuildConfig.class, packageName = "com.example.hp.sibujang_app")
@RunWith(RobolectricTestRunner.class)
public class MainActivityTest {
    SessionManager session;
    private LoginActivity activityLogin;
    private MainActivity activity;
    BottomNavigationView navigation;

    @Before
    public void setup() throws Exception {
        activityLogin = Robolectric.setupActivity(LoginActivity.class);
        session = new SessionManager(activityLogin.getApplicationContext());
        session.createLoginSession("PY-368", "Penyuluh", "000-368");
        activity = Robolectric.setupActivity(MainActivity.class);
        navigation  = (BottomNavigationView) activity.findViewById(R.id.navigation);
    }

    @Test
    public void checkActivityNotNull() throws Exception {
        assertNotNull("How come MainActivity is null???", activity);
    }

    @RequiresApi(api = Build.VERSION_CODES.CUPCAKE)
    @Test
    public void validateNavigation() {
        BottomNavigationView navigation = (BottomNavigationView) activity.findViewById(R.id.navigation);
        assertNotNull("Navigation could not be found", navigation);
        assertNotNull(navigation.getSelectedItemId());
    }

    @Test
    public void testOptionsMenu() throws Exception {
        assertNotNull(activity.getLayoutInflater());
        assertNotNull(activity.getMenuInflater());
    }
//
//    @Test
//    public void testOnPause(){
//        activity.onPause();
//        LocalBroadcastManager.getInstance(activity).unregisterReceiver(activity.mRegistrationBroadcastReceiver);
//    }
//
//    @Test
//    public void testOnResume(){
//        activity.onResume();
//        LocalBroadcastManager.getInstance(activity).registerReceiver(activity.mRegistrationBroadcastReceiver
//        , new IntentFilter(RegistrationService.REGISTRATION_SUCCESS));
//        LocalBroadcastManager.getInstance(activity).registerReceiver(activity.mRegistrationBroadcastReceiver
//                , new IntentFilter(RegistrationService.REGISTRATION_ERROR));
//    }
}
