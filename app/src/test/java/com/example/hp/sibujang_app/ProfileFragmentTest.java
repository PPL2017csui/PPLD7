package com.example.hp.sibujang_app;

/**
 * Created by Sabiq on 26/04/2017.
 */

import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import org.apache.tools.ant.Main;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertNotNull;
import static org.robolectric.util.FragmentTestUtil.startFragment;

import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.support.v4.SupportFragmentTestUtil;
import org.robolectric.util.FragmentTestUtil;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

@Config(constants = BuildConfig.class, packageName = "com.example.hp.sibujang_app")
@RunWith(RobolectricTestRunner.class)
public class ProfileFragmentTest{
    private MainActivity activity;
    private ProfileFragment profileFragment;
    private Staff profile;
    private ListView listView;
    private ProfileAdapter adapter;
    private ArrayList<String> data;

    @Before
    public void setUp() throws Exception {
        data = new ArrayList<String>();
        data.add("58dab7b786450c413d083364");
        data.add("000-375");
        data.add("PY 375");
        data.add("93");
        data.add("KARAWANG");
        data.add("BMFO-73");
        profile = new Staff(""+data.get(0), ""+data.get(1), ""+data.get(2), ""+data.get(3), ""+data.get(4), ""+data.get(5));
        activity = Robolectric.buildActivity(MainActivity.class).create().get();
        adapter = new ProfileAdapter(activity, data);
        LayoutInflater inflater = activity.getLayoutInflater();
        View view = inflater.inflate(R.layout.fragment_profile, null, false);
        listView = (ListView) view.findViewById(R.id.list_view);
        assertNotNull(listView);
        listView.setAdapter(adapter);
        assertNotNull(adapter);
      //  activity = Robolectric.setupActivity(MainActivity.class);
        profileFragment = new ProfileFragment();
      //  SupportFragmentTestUtil.startFragment(profileFragment, activity.getClass());
    }

    @Test
    public void shouldNotBeNull() throws Exception
    {
   //     assertNotNull(profileFragment);
    }

    @Test
    public void validateNewInstance() throws Exception
    {
     //   assertNotNull(ProfileFragment.newInstance());
    }


}

