package com.example.hp.sibujang_app;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;

import com.example.hp.sibujang_app.notification.NotificationsListenerService;
import com.example.hp.sibujang_app.notification.RegistrationService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

/**
 * Created by Ismatullah on 27/04/2017.
 */
@Config(constants = BuildConfig.class, packageName = "com.example.hp.sibujang_app")
@RunWith(RobolectricTestRunner.class)
public class NotificationListenerTest {
    private NotificationsListenerService notification;

    @Before
    public void setup() throws Exception {
        notification = Robolectric.setupService(NotificationsListenerService.class);
    }

    @Test
    public void checkIntentServiceNotNull() throws Exception {
        assertNotNull("How come Notification Listener is null???", notification);
    }

    @Test
    public void sendNotificationTest() throws Exception {
        Intent intent = new Intent(notification, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        assertEquals(Intent.FLAG_ACTIVITY_CLEAR_TOP, intent.getFlags());
        int requestCode = 0;
        PendingIntent pendingIntent = PendingIntent.getActivity(notification, requestCode, intent, PendingIntent.FLAG_ONE_SHOT);
        Uri sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder notifBuilder = new NotificationCompat.Builder(notification)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent)
                .setSound(sound);
        assertNotNull(notifBuilder);
        NotificationManager notificationManager = (NotificationManager) notification.getSystemService(Context.NOTIFICATION_SERVICE);
        assertNotNull(notificationManager);
        Bundle bundle = new Bundle();
        notification.onMessageReceived("Test", bundle);
        notification.sendNotification("Test", "Test2");
        assertNotNull(notification.getApplicationContext());
    }


}
