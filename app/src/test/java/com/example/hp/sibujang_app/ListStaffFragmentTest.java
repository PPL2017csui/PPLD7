package com.example.hp.sibujang_app;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListView;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by HP on 04/05/2017.
 */
@Config(constants = BuildConfig.class, packageName = "com.example.hp.sibujang_app")
@RunWith(RobolectricTestRunner.class)
public class ListStaffFragmentTest {
    private MainActivity activity;
    private ListStaffFragment fragment;
    private Staff profile;
    private RecyclerView recView;
    private ListStaffAdapter adapter;
    private List<String> data;
    private List<Staff> dataUser;
    @Before
    public void setUp() throws Exception {
        dataUser = new ArrayList<Staff>();
        profile = new Staff("58dab7b786450c413d083364", "000-375", "PY 375", ""+"93", "KARAWANG", "BMFO-73");
        dataUser.add(profile);
        activity = Robolectric.buildActivity(MainActivity.class).create().get();
        adapter = new ListStaffAdapter(activity, dataUser);
        LayoutInflater inflater = activity.getLayoutInflater();
        View view = inflater.inflate(R.layout.fragment_list_staff, null, false);
        recView = (RecyclerView) view.findViewById(R.id.recycler_view_viewstaff);
        recView.setAdapter(adapter);
        assertNotNull(adapter);
        fragment = new ListStaffFragment();
    }

    @Test
    public void shouldNotBeNull() throws Exception
    {
             assertNotNull(fragment);
    }

    @Test
    public void validateNewInstance() throws Exception
    {
        //   assertNotNull(ProfileFragment.newInstance());
    }


}