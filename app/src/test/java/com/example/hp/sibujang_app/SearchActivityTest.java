package com.example.hp.sibujang_app;

import android.app.DatePickerDialog;
import android.widget.Button;
import android.widget.ImageButton;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowDatePickerDialog;

import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertEquals;

/**
 * Created by Sabiq on
 */
@Config(constants = BuildConfig.class, packageName = "com.example.hp.sibujang_app")
@RunWith(RobolectricTestRunner.class)
public class SearchActivityTest {
    private SearchActivity activity;
    private Button cariBtn;
    private ImageButton pickDate;

    @Before
    public void setup() throws Exception {
        activity = Robolectric.setupActivity(SearchActivity.class);
    }

    @Test
    public void checkActivityNotNull() throws Exception {
        assertNotNull("How come MainActivity is null???", activity);
    }

//    @Test
//    public void validatePickDate() throws Exception {
//        pickDate = (ImageButton) activity.findViewById(R.id.pickDateButton);
//        pickDate.performClick();
//        DatePickerDialog dialog = (DatePickerDialog) ShadowDatePickerDialog.getLatestDialog();
//        dialog.updateDate(2017, 01, 01);
//        dialog.getButton(DatePickerDialog.BUTTON_POSITIVE).performClick();
////        assertEquals("2013-11-23", startDate.getText().toString());
//        assertNotNull("How come MainActivity is null???", activity);
//    }

    @Test
    public void validateCariButton() throws Exception {
        cariBtn = (Button) activity.findViewById(R.id.cari);
        cariBtn.callOnClick();
        assertNotNull("How come MainActivity is null???", activity);
    }
}
