package com.example.hp.sibujang_app;

import android.content.Intent;
import android.widget.FrameLayout;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Ismatullah on 28/05/2017.
 */
@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class,packageName = "com.example.hp.sibujang_app")
public class SearchTabAdapterTest {
    ResultSearchTimActivity activity;
    SearchTabAdapter adapter;
    private List<Staff> my_data;

    @Before
    public void setUp() throws Exception {
        Intent intent = new Intent();
        intent.putExtra("namaStaff","Andi Malarangeng");
        activity = Robolectric.buildActivity(ResultSearchTimActivity.class).withIntent(intent).create().get();
        String id = "123456";
        String name = "Andi Malarangeng";
        String code = "000-372";
        String branchCode = "92";
        String branchName = "BANDUNG LAUTAN API";
        String BMFOCode = "BMFO-72";
        my_data = new ArrayList<Staff>();
        my_data.add(new Staff(id, code, name, branchCode, branchName, BMFOCode));
        adapter = new SearchTabAdapter(activity.getApplicationContext(), my_data);
    }

    @Test
    public void checkActivityNotNull() throws Exception {
        assertNotNull("How come ResultSearchActivity is null???", activity);
    }

    @Test
    public void onCreateViewHolder() throws Exception {
        SearchTabAdapter.ViewHolder viewHolder = adapter.onCreateViewHolder(
                new FrameLayout(RuntimeEnvironment.application), 0);
        assertNotNull(viewHolder);
    }

    @Test
    public void onBindViewHolder() throws Exception {
        SearchTabAdapter.ViewHolder viewHolder = adapter.onCreateViewHolder(
                new FrameLayout(RuntimeEnvironment.application), 0);
        adapter.onBindViewHolder(viewHolder, 0);
        assertEquals("Andi Malarangeng", viewHolder.name.getText());
        assertEquals("000-372", viewHolder.code.getText());
    }

    @Test
    public void getItemCount() throws Exception {
        assertTrue("Item List contains incorrect count",
                my_data.size() == adapter.getItemCount());
    }

    @Test
    public void performClick() throws Exception
    {
        SearchTabAdapter.ViewHolder viewHolder = adapter.onCreateViewHolder(
                new FrameLayout(RuntimeEnvironment.application), 0);
        int position = viewHolder.getAdapterPosition();
        // Staff newStaff = my_data.get(position);
        // assertEquals("Andi Malarangeng", newStaff.getName());
    }


}