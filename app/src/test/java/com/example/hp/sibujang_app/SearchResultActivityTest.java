package com.example.hp.sibujang_app;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;

/**
 * Created by Sabiq
 */

@Config(constants = BuildConfig.class, packageName = "com.example.hp.sibujang_app")
@RunWith(RobolectricTestRunner.class)
public class SearchResultActivityTest {
    private SearchResultActivity activity;
    private String namaPencapaian;
    private String tanggalPencapaian;

    @Before
    public void setup() throws Exception {
        Intent intent = new Intent();
        intent.putExtra("namaPencapaian","KPI");
        intent.putExtra("tanggalPencapaian","2017-1-1");
        activity = Robolectric.buildActivity(SearchResultActivity.class).withIntent(intent).create().get();
    }

    @Test
    public void checkActivityNotNull() throws Exception {
        assertNotNull("How come MainActivity is null???", activity);
    }

    @Test
    public void validateRecycler() {
        RecyclerView recyclerView = (RecyclerView) activity.findViewById(R.id.recycler_view_viewachievement);
        Assert.assertNotNull(recyclerView);
        assertTrue(recyclerView.getLayoutManager().canScrollVertically());
        Assert.assertNotNull(recyclerView.getAdapter());
    }
}
