package com.example.hp.sibujang_app;

import org.junit.*;

import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.lang.reflect.Array;
import java.util.ArrayList;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Class representation for Achievement unit test
 * Created by Pie-O, 03-29-2017
 */
@Config(constants = BuildConfig.class, packageName = "com.example.hp.sibujang_app")
@RunWith(RobolectricTestRunner.class)
public class AchievementTest {

    private Achievement achievements;

    private String type = "Kunjungan";
    private String date = "2017-01-01";
    private String attribute = "Target";
    private String value = "50";
    private ArrayList<String> attributes= new ArrayList<>();

    @Before
    public void setup() {
        attributes.add(attribute + ": " + value);
        achievements = new Achievement(
                type, date, attributes);
    }

    @Test
    public void typeCheck() {
        Achievement achievement = Mockito.mock(Achievement.class);
        when(achievement.getType()).thenReturn("Kunjungan");
    }

    @Test
    public void dateCheck() {
        Achievement achievement = Mockito.mock(Achievement.class);
        when(achievement.getDate()).thenReturn("Sunday, 1 Jan 2017");
    }

    @Test
    public void dataCheck() {
        assertEquals(1, achievements.getData().size());
        assertEquals("Target: 50", achievements.getData().get(0));
    }
}