package com.example.hp.sibujang_app;

import android.app.Instrumentation;
import android.support.test.InstrumentationRegistry;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.InstrumentationRegistry.getContext;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withEffectiveVisibility;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.junit.Assert.*;
import android.app.ProgressDialog;
import android.content.Context;

import java.net.HttpURLConnection;
import java.net.URL;
import com.example.hp.sibujang_app.MainActivity;
import com.example.hp.sibujang_app.ListAchievementFragment.RetrieveDB;

/**
 * Class Representation for Callback
 */
@RunWith(AndroidJUnit4.class)
@LargeTest
public class RetrieveDBTest {
    private MainActivity main;
    private URL url;
    private ProgressDialog dialog;

    /**@Rule
    public ActivityTestRule<RetrieveDB> mActivityRule = new ActivityTestRule<>(MainActivity.RetrieveDB.class);
    */
    @Before
    public void setUp() throws Exception {
        /**main = (RetrieveDB) mActivityRule.getActivity();
        retrieveActivity = main.RetrieveDB.init();
        dialog = new ProgressDialog(main.getApplicationContext());*/
        main = new MainActivity();
        dialog = new ProgressDialog(main.getApplicationContext());
    }

    @Test
    public void testPreExecute() throws Exception {
        dialog.isShowing();
    }

    @Test
    public void testBackground() throws Exception {
        url = new URL("https://sibujang-app-staging.herokuapp.com/api/achievements/000-2/2017-01-01");
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        assertEquals("GET", urlConnection.getRequestMethod());
        assertEquals(200, urlConnection.getResponseCode());
        assertEquals(10000, urlConnection.getConnectTimeout());
        assertEquals(10000, urlConnection.getReadTimeout());
        assertEquals("application/json", urlConnection.getRequestProperty("Content-type"));
    }

    @Test
    public void testPostExecute() throws Exception {

    }


}