package com.example.hp.sibujang_app;

import com.example.hp.sibujang_app.LoginActivity;
import com.example.hp.sibujang_app.MainActivity;
import android.app.Activity;
import android.app.Instrumentation;
import android.app.Instrumentation.ActivityMonitor;
import android.test.ActivityInstrumentationTestCase2;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.test.InstrumentationRegistry;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.ActivityInstrumentationTestCase2;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import static android.app.PendingIntent.getActivity;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static junit.framework.Assert.assertNotNull;

/**
 * Class Representation for Instrumented Test of LoginActivity
 */
@RunWith(AndroidJUnit4.class)
@LargeTest
public class LoginActivityInstrumentedTest extends ActivityInstrumentationTestCase2<LoginActivity>{
    @Rule
    public ActivityTestRule rule = new ActivityTestRule<>(
            LoginActivity.class);

    private LoginActivity testActivity;
    private EditText username;
    private EditText password;
    private Button loginButton;

    public LoginActivityInstrumentedTest() {
        super(LoginActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        // Starts the activity under test using
        // the default Intent with:
        // action = {@link Intent#ACTION_MAIN}
        // flags = {@link Intent#FLAG_ACTIVITY_NEW_TASK}
        // All other fields are null or empty.
        testActivity = getActivity();
        username = (EditText) testActivity.findViewById(R.id.userName);
        password = (EditText) testActivity.findViewById(R.id.password);
        loginButton = (Button) testActivity.findViewById(R.id.bLogin);
    }

    /**
     * Test if your test fixture has been set up correctly.
     * You should always implement a test that
     * checks the correct setup of your test fixture.
     * If this tests fails all other tests are
     * likely to fail as well.
     */
    public void testPreconditions() {
        // Try to add a message to add context to your assertions.
        // These messages will be shown if
        // a tests fails and make it easy to
        // understand why a test failed
        assertNotNull("mTestActivity is null", testActivity);
        assertNotNull("mTestEmptyText is null", username);
        assertNotNull("mTestEmptyText is null", password);
        assertNotNull("mTestEmptyText is null", loginButton);
    }

    @Test
    public void checkLogin() {
        String uname = "username";
        String pwd = "password";

        //find the username edit text and type in the username
        onView(withId(R.id.userName)).perform(typeText(uname), closeSoftKeyboard());

        //find the password edit text and type in the password
        onView(withId(R.id.password)).perform(typeText(pwd), closeSoftKeyboard());

        //click the signin button
        onView(withId(R.id.bLogin)).perform(click());

        //check that the app display the main activity after login
        Intent intent = new Intent(testActivity, MainActivity.class);
        rule.launchActivity(intent);
        //onView(withId(R.id.navigation)).check(matches(isDisplayed()));
    }
}
