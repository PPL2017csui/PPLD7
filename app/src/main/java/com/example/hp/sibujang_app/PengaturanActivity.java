package com.example.hp.sibujang_app;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;

public class PengaturanActivity extends AppCompatActivity {
    SessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pengaturan);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Pengaturan");
        toolbar.setTitleTextColor(0xFFFFFFFF);
        session = new SessionManager(getApplicationContext());

        final Switch bSwitch = (Switch) findViewById(R.id.switchs);
        final Button bUbahPwd = (Button) findViewById(R.id.ubah_password);

        if(session.getStatusNotif()){
            bSwitch.setChecked(true);
        }

        bSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    session.setNotifOn();
                } else {
                    session.setNotifOff();
                }
            }
        });

        bUbahPwd.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), UbahPasswordActivity.class);
                startActivity(intent);
            }
        });
    }



}
