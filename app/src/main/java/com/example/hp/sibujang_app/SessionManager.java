package com.example.hp.sibujang_app;

/**
 * Created by USER on 5/17/2017.
 */
import java.util.HashMap;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class SessionManager {
    // Shared Preferences
    SharedPreferences pref;

    // Editor for Shared preferences
    Editor editor;

    // Context
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Sharedpref file name
    private static final String PREF_NAME = "session";

    // All Shared Preferences Keys
    private static final String IS_LOGIN = "IsLoggedIn";

    // User name (make variable public to access from outside)
    public static final String KEY_USERNAME = "username";

    // Position (make variable public to access from outside)
    public static final String KEY_POSITION = "position";

    // Kode Staff (make variable public to access from outside)
    public static final String KEY_KODESTAFF = "kodeStaff";

    // notifOff/onn (make variable public to access from outside)
    public static final String KEY_NOTIFIKASI = "notifikasi";

    // Constructor
    public SessionManager(Context context){
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    /**
     * Create login session
     * */
    public void createLoginSession(String name, String position, String kodeStaff){
        // Storing login value as TRUE
        editor.putBoolean(IS_LOGIN, true);

        // Storing name in pref
        editor.putString(KEY_USERNAME, name);

        // Storing name in pref
        editor.putString(KEY_POSITION, position);

        // Storing name in pref
        editor.putString(KEY_KODESTAFF, kodeStaff);

        editor.putBoolean(KEY_NOTIFIKASI, false);

        // commit changes
        editor.commit();
    }

    /**
     * Check login method wil check user login status
     * If false it will redirect user to login page
     * Else won't do anything
     * */
    public void checkLogin(){
        // Check login status
        if(!this.isLoggedIn()){
            // user is not logged in redirect him to Login Activity
            Intent i = new Intent(_context, LoginActivity.class);
            // Closing all the Activities
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            // Add new Flag to start new Activity
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            // Staring Login Activity
            _context.startActivity(i);
        }
    }



    /**
     * Get stored session data
     * */
    public HashMap<String, String> getUserDetails(){
        HashMap<String, String> user = new HashMap<String, String>();
        // user name
        user.put(KEY_USERNAME, pref.getString(KEY_USERNAME, null));

        // position
        user.put(KEY_POSITION, pref.getString(KEY_POSITION, null));

        // kode staff
        user.put(KEY_KODESTAFF, pref.getString(KEY_KODESTAFF, null));

        // return user
        return user;
    }

    public boolean getStatusNotif(){
        return pref.getBoolean(KEY_NOTIFIKASI, false);
    }

    public void setNotifOff(){
        editor.remove(KEY_NOTIFIKASI);
        editor.putBoolean(KEY_NOTIFIKASI, false);
        editor.commit();
    }

    public void setNotifOn(){
        editor.remove(KEY_NOTIFIKASI);
        editor.putBoolean(KEY_NOTIFIKASI, true);
        editor.commit();
    }

    /**
     * Clear session details
     * */
    public void logoutUser(){
        // Clearing all data from Shared Preferences
        editor.clear();
        editor.commit();

        // After logout redirect user to Loing Activity
        Intent i = new Intent(_context, LoginActivity.class);
        // Closing all the Activities
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        // Add new Flag to start new Activity
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        // Staring Login Activity
        _context.startActivity(i);
    }

    /**
     * Quick check for login
     * **/
    // Get Login State
    public boolean isLoggedIn(){
        return pref.getBoolean(IS_LOGIN, false);
    }
}
