package com.example.hp.sibujang_app;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class SearchResultActivity extends AppCompatActivity {
    private String namaPencapaian;
    private String tanggalPencapaian;
    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;
    private SearchResultAdapter adapter;
    private List<Achievement> data_list;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_result);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setTitle( Html.fromHtml("<font color='#ffffff'>Hasil Pencarian</font>"));
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view_viewachievement);
        linearLayoutManager = new LinearLayoutManager(SearchResultActivity.this, LinearLayoutManager.VERTICAL, false);
        SharedPreferences prefs = getSharedPreferences("myPref", MODE_PRIVATE);
        namaPencapaian = prefs.getString("namaPencapaian", "No name defined");
        tanggalPencapaian = prefs.getString("tanggalPencapaian", "No name defined");
        recyclerView.setLayoutManager(linearLayoutManager);
        new RetrieveDB().execute("https://sibujang-app-staging.herokuapp.com/api/achievements/000-1/"+tanggalPencapaian+"/"+namaPencapaian);
        data_list  = new ArrayList<>();
        adapter = new SearchResultAdapter(SearchResultActivity.this,data_list,namaPencapaian, tanggalPencapaian);
        recyclerView.setAdapter(adapter);
    }



    class RetrieveDB extends AsyncTask<String, Void, String> {
        Intent intent;
        ProgressDialog progressDialog;
        @Override
        protected void onPreExecute(){
            super.onPreExecute();
            progressDialog = new ProgressDialog(SearchResultActivity.this);
            progressDialog.setMessage("Loading data...");
            progressDialog.show();
        }
        @Override
        protected String doInBackground(String... params){

            StringBuilder result = new StringBuilder();
            String data = "";

            try{
                //Connect to API
                URL url = new URL(params[0]);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setReadTimeout(10000);
                urlConnection.setConnectTimeout(10000);
                urlConnection.setRequestMethod("GET");
                urlConnection.setRequestProperty("Content-type", "application/json");
                urlConnection.connect();

                //Read API return value
                InputStream inputStream = urlConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String line;
                while((line = bufferedReader.readLine()) != null ){
                    result.append(line).append("\n");
                }
                data = result.toString();

            }catch (IOException ex) {
            }
            return data;
        }


        @RequiresApi(api = Build.VERSION_CODES.CUPCAKE)
        @Override
        protected void onPostExecute(String result){
            super.onPostExecute(result);
            SharedPreferences prefs = getSharedPreferences("myPref", MODE_PRIVATE);
            try {
                JSONArray myObjects = new JSONArray(result);
                for (int i=0; i<myObjects.length(); i++){
                    JSONObject object = myObjects.getJSONObject(i);
                    ArrayList<String> dataAchievement= new ArrayList<>();
                    Iterator keys = object.keys();
                    while(keys.hasNext()) {
                        String attributeName= (String)keys.next();
                        String value=  (object.get(attributeName)).toString();
                        dataAchievement.add(attributeName+":"+value);
                    }
                    Achievement data = new Achievement(object.getString("jenis"),object.getString("tanggal"),dataAchievement);
                    data_list.add(data);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            adapter.notifyDataSetChanged();
            if(progressDialog != null){
                if(result.equalsIgnoreCase("[]\n") || prefs.getString("isNull", "No name defined").equalsIgnoreCase("false")){

                    Toast.makeText(SearchResultActivity.this, "Hasil Pencarian Kosong", Toast.LENGTH_LONG).show();
                    intent = new Intent(SearchResultActivity.this, SearchActivity.class);
                    startActivity(intent);
                }
//                    Toast.makeText(SearchResultActivity.this, "Hasil Pencarian Kosong", Toast.LENGTH_LONG).show();
                progressDialog.dismiss();
            }
        }
    }
}
