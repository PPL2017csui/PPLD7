package com.example.hp.sibujang_app;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

//import com.bumptech.glide.Glide;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;
/**
 * Created by HP on 29/03/2017.
 */

public class SummaryCustomAdapter extends RecyclerView.Adapter<SummaryCustomAdapter.ViewHolder> {

    private Context context;
    private List<Achievement> myData;

    public SummaryCustomAdapter(Context context, List<Achievement> my_data) {
        this.context = context;
        this.myData = my_data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card,parent,false);
        return new ViewHolder(itemView,this.context,this.myData);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.achievement_date.setText(myData.get(position).getMonth());
        holder.achievement_type.setText(myData.get(position).getType());
        if(myData.get(position).getType().equals("Kunjungan")){
            holder.icon.setImageResource(R.drawable.kunjungan);
        }
        else
        {
            holder.icon.setImageResource(R.drawable.kpi);
        }
    }


    @Override
    public int getItemCount() {
        return myData.size();
    }
    public Context getContext(){
        return context;
    }


    public class ViewHolder extends  RecyclerView.ViewHolder implements View.OnClickListener{

        public TextView achievement_date;
        public TextView achievement_type;
        public ImageView icon;
        private String summaryTitle;
        private Context context;
        private List<Achievement> myData;

        public ViewHolder(View itemView, Context context, List<Achievement> my_data) {
            super(itemView);
            this.myData=my_data;
            this.context=context;
            achievement_date = (TextView) itemView.findViewById(R.id.Achievement_date);
            achievement_type = (TextView) itemView.findViewById(R.id.Achievement_type);
            icon = (ImageView) itemView.findViewById(R.id.achievement_icon);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position= getAdapterPosition();
            Achievement objectSummary = this.myData.get(position);
            summaryTitle=objectSummary.getType();
            Intent intent = new Intent(this.context, DetailSummary.class);

            intent.putStringArrayListExtra("summaryAttribute",(ArrayList<String>) objectSummary.getData());
            intent.putExtra("summaryTitle",summaryTitle);
            intent.putExtra("summaryType",objectSummary.getType());
            intent.putExtra("summaryMonth",objectSummary.getMonth());

            context.startActivity(intent);
        }
    }
}