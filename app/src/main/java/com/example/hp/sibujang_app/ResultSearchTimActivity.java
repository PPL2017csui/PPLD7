package com.example.hp.sibujang_app;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ResultSearchTimActivity extends AppCompatActivity {
    private String wilayah;
    private String kodeStaff;
    private String backWil;
    private String backName;
    private String flagWilayah;
    private String flagName;
    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;
    private SearchTabAdapter adapter;
    protected List<Staff> data_list;
    private Toolbar toolbar;
    private int totalPencapaian;
    private String kodeBMFO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        data_list  = new ArrayList<>();
        setContentView(R.layout.activity_result_search_tim);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setTitle( Html.fromHtml("<font color='#ffffff'>Hasil Pencarian</font>"));
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view_viewstaff);
        linearLayoutManager = new LinearLayoutManager(ResultSearchTimActivity.this, LinearLayoutManager.VERTICAL, false);

        SharedPreferences prefs = getSharedPreferences("myPref", MODE_PRIVATE);
        wilayah = prefs.getString("wilayah", "No staff name defined");
        kodeStaff = prefs.getString("kodeStaff", "No location defined");
        flagWilayah = prefs.getString("flagWilayah", "No flagWilayah defined");
        flagName = prefs.getString("flagName", "No flagName defined");
        kodeBMFO = prefs.getString("codeLocation", "No flagName defined");
        adapter = new SearchTabAdapter(ResultSearchTimActivity.this,data_list);
        backWil = prefs.getString("backWil", "No backWil defined");
        backName = prefs.getString("backName", "No backName defined");
        recyclerView.setAdapter(adapter);
        SharedPreferences.Editor editor = getSharedPreferences("myPref", MODE_PRIVATE).edit();
        recyclerView.setLayoutManager(linearLayoutManager);

        if (flagWilayah.equals("1") || backWil.equals("0")) {
            new RetrieveDB().execute("https://sibujang-app-staging.herokuapp.com/api/staffs/bmfo/"+kodeBMFO);
            editor.putString("flagWilayah", "0");
            editor.putString("backWil", "1");
        }
        else if (flagName.equals("1") || backName.equals("0")) {
            new RetrieveDB().execute("https://sibujang-app-staging.herokuapp.com/api/staffs/"+kodeStaff);
            editor.putString("flagName", "0");
            editor.putString("backName", "1");
        }
        editor.commit();
    }
    @Override
    public void onBackPressed() {
        // do something on back.
        Intent intent = new Intent(this , SearchTimLapangan.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        return;
    }


    class RetrieveDB extends AsyncTask<String, Void, String> {

        ProgressDialog progressDialog;
        @Override
        protected void onPreExecute(){
            super.onPreExecute();
            progressDialog = new ProgressDialog(ResultSearchTimActivity.this);
            progressDialog.setMessage("Loading data...");
            progressDialog.show();
        }
        @Override
        protected String doInBackground(String... params){

            StringBuilder result = new StringBuilder();
            String data = "";

            try{
                //Connect to API
                URL url = new URL(params[0]);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setReadTimeout(10000);
                urlConnection.setConnectTimeout(10000);
                urlConnection.setRequestMethod("GET");
                urlConnection.setRequestProperty("Content-type", "application/json");
                urlConnection.connect();

                //Read API return value
                InputStream inputStream = urlConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String line;
                while((line = bufferedReader.readLine()) != null ){
                    result.append(line).append("\n");
                }
                data = result.toString();

            }catch (IOException ex) {
            }
            return data;
        }


        @RequiresApi(api = Build.VERSION_CODES.CUPCAKE)
        @Override
        protected void onPostExecute(String result){
            super.onPostExecute(result);
            try {
                if (flagWilayah.equals("1") || backWil.equals("0")) {
                    JSONArray myObjects = new JSONArray(result);
                    for (int i=0; i<myObjects.length(); i++) {
                        JSONObject object = myObjects.getJSONObject(i);
                        Staff data = new Staff(object.getString("_id"), object.getString("kodeStaff"), object.getString("namaStaff"),
                                object.getString("kodeBranch"), object.getString("namaBranch"), object.getString("kodeBMFO"));
                        data_list.add(data);
                    }
                }
                else if (flagName.equals("1") || backName.equals("0")) {
                    JSONObject object = new JSONObject(result);
                     Staff data = new Staff(object.getString("_id"), object.getString("kodeStaff"), object.getString("namaStaff"),
                            object.getString("kodeBranch"), object.getString("namaBranch"), object.getString("kodeBMFO"));
                    data_list.add(data);
                }
                totalPencapaian = data_list.size();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            adapter.notifyDataSetChanged();
            if(progressDialog != null){
                if(result.equalsIgnoreCase("[]\n"))
                    Toast.makeText(ResultSearchTimActivity.this, "Hasil Pencarian Kosong", Toast.LENGTH_LONG).show();
                progressDialog.dismiss();
            }
        }
    }
}
