package com.example.hp.sibujang_app;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;

public class SearchActivity extends AppCompatActivity {
    private Toolbar toolbar;
    ImageButton btn;
    Spinner spinner;
    EditText editText;
    ArrayAdapter<CharSequence> adapter;
    int year_x, month_x, day_x;
    static final int DIALOG_ID = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        setContentView(R.layout.activity_search);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        SharedPreferences.Editor editor = getSharedPreferences("myPref", MODE_PRIVATE).edit();
        editor.putString("isNull", "FALSE");
        editor.commit();
        setTitle( Html.fromHtml("<font color='#ffffff'>Cari Pencapaian</font>"));
        spinner = (Spinner) findViewById(R.id.planets_spinner);
        adapter = ArrayAdapter.createFromResource(this, R.array.search_spinner, android.R.layout.simple_spinner_dropdown_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        editText = (EditText) findViewById(R.id.dateText);
        final Calendar cal = Calendar.getInstance();
        year_x = cal.get(Calendar.YEAR);
        month_x = cal.get(Calendar.MONTH);
        day_x = cal.get(Calendar.DAY_OF_MONTH);
        SharedPreferences prefs = getSharedPreferences("myPref", MODE_PRIVATE);
        if(prefs.getString("namaPencapaian", "No name defined").equalsIgnoreCase("false")){
            Toast.makeText(SearchActivity.this, "Tanggal belum diisi", Toast.LENGTH_LONG).show();
        }
        showDialogOnButtonClick();
    }

    public void showDialogOnButtonClick(){
        btn = (ImageButton) findViewById(R.id.pickDateButton);

        btn.setOnClickListener(
                new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        showDialog(DIALOG_ID);
                    }
                }
        );
    }

    @Override
    protected Dialog onCreateDialog(int id){
        if(id == DIALOG_ID)
            return new DatePickerDialog(this, dpickerListener, year_x, month_x, day_x);
        return null;
    }

    private DatePickerDialog.OnDateSetListener dpickerListener = new DatePickerDialog.OnDateSetListener(){
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth){
            year_x = year;
            month_x = monthOfYear+1;
            day_x = dayOfMonth;
            editText.setText(year_x+"-"+month_x+"-"+day_x, TextView.BufferType.EDITABLE);
        }
    };

    public void searchAction(View v){
        String namaPencapaian = spinner.getSelectedItem().toString();
        String tanggalPencapaian = editText.getText().toString();
        SharedPreferences.Editor editor = getSharedPreferences("myPref", MODE_PRIVATE).edit();
        if(tanggalPencapaian.equalsIgnoreCase("")){
            Toast.makeText(SearchActivity.this, "Tanggal belum diisi", Toast.LENGTH_LONG).show();
        }else {
//            Toast.makeText(SearchActivity.this, "" + (!TextUtils.isEmpty(editText.getText().toString())), Toast.LENGTH_LONG).show();
            editor.putString("isNull", "" + (!TextUtils.isEmpty(editText.getText().toString())));
            Intent intent = new Intent(SearchActivity.this, SearchResultActivity.class);
            editor.putString("namaPencapaian", namaPencapaian);
            editor.putString("tanggalPencapaian", tanggalPencapaian);

            editor.commit();
            startActivity(intent);
        }
    }
}
