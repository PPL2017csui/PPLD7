package com.example.hp.sibujang_app;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

//import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;
/**
 * Created by HP on 29/03/2017.
 */

public class ListStaffAdapter extends RecyclerView.Adapter<ListStaffAdapter.ViewHolder> {

    private Context context;
    private List<Staff> my_data;


    public ListStaffAdapter(Context context, List<Staff> my_data) {
        this.context = context;
        this.my_data = my_data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_staff_card,parent,false);

        return new ViewHolder(itemView,this.context,this.my_data);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.staff_name.setText(my_data.get(position).getName());
        holder.staff_code.setText(my_data.get(position).getCode());
    }


    @Override
    public int getItemCount() {
        return my_data.size();
    }
    public Context getContext(){
        return context;
    }


    public  class ViewHolder extends  RecyclerView.ViewHolder implements View.OnClickListener{

        public TextView staff_name;
        public TextView staff_code;
        private String staffName;
        private Context context;
        private List<Staff> my_data;

        public ViewHolder(View itemView, Context context, List<Staff> my_data) {
            super(itemView);
            this.my_data=my_data;
            this.context=context;
            staff_name = (TextView) itemView.findViewById(R.id.staff_name);
            staff_code = (TextView) itemView.findViewById(R.id.staff_code);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position= getAdapterPosition();
            Staff objectStaff = this.my_data.get(position);
            staffName=objectStaff.getName();
            Intent intent = new Intent(this.context, DetailStaff.class);
            intent.putExtra("staffName",staffName);
            intent.putStringArrayListExtra("staffData",(ArrayList<String>) objectStaff.getData());
            context.startActivity(intent);
        }
    }
}
