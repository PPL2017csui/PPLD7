package com.example.hp.sibujang_app;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class DetailSummary extends AppCompatActivity {
    private ListView listView;
    private String summaryTitle;
    private String summaryType;
    private String summaryMonth;


    private ArrayList<String> detailSummary;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_summary);
        // Construct the data source
        summaryTitle = getIntent().getStringExtra("summaryTitle");
        detailSummary = getIntent().getStringArrayListExtra("summaryAttribute");
        summaryType = getIntent().getStringExtra("summaryType");
        summaryMonth = getIntent().getStringExtra("summaryMonth");


        // Create the adapter to convert the array to views
        setTitle( Html.fromHtml("<font color='#ffffff'>Detil Ringkasan</font>"));
        DetailAchievementAdapter adapter = new DetailAchievementAdapter(this, detailSummary);

        String[] achievementTitleSplit = summaryTitle.split("\\(");
        TextView windowTitle = (TextView) findViewById(R.id.textView5);
        windowTitle.setText(summaryType);

        TextView windowDate = (TextView) findViewById(R.id.textView6);
        windowDate.setText(summaryMonth);

        TextView windowType = (TextView) findViewById(R.id.textView8);
        windowType.setText("Bulanan");

        // Attach the adapter to a ListView
        ListView listView = (ListView) findViewById(R.id.achievement_list_view);
        listView.setAdapter(adapter);
    }
}
