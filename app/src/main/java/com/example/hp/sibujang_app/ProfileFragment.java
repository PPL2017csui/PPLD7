package com.example.hp.sibujang_app;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import io.fabric.sdk.android.services.concurrency.AsyncTask;

public class ProfileFragment extends Fragment {
    private ListView listView;
    TextView mtext;
    TextView nameText;
    TextView codeText;
    private Staff profile;
    private ArrayList profileToArList;
    private Context ctx;
    View mView;
    SessionManager session;
    String kodeStaff;

    public static ProfileFragment newInstance() {
        ProfileFragment fragment = new ProfileFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //initialize session
        session = new SessionManager(getActivity());
        //add this to check login
        session.checkLogin();
        // get user data from session
        HashMap<String, String> user = session.getUserDetails();

        // get string kodestaff
        kodeStaff = user.get(SessionManager.KEY_KODESTAFF);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mView = inflater.inflate(R.layout.fragment_profile, container, false);
        mtext = (TextView) mView.findViewById(R.id.profil_name);
        nameText = (TextView) mView.findViewById(R.id.profil_name);
        codeText = (TextView) mView.findViewById(R.id.profil_code);
        mtext.setText("Loading Data...");
        profileToArList= new ArrayList<String>();
        ctx = getActivity().getApplicationContext();
        new RetrieveDB().execute("https://sibujang-app-staging.herokuapp.com/api/staffs/"+kodeStaff);
        return mView;
    }

    class RetrieveDB extends AsyncTask<String, Void, String> {

        ProgressDialog progressDialog;
        @Override
        protected void onPreExecute(){
            super.onPreExecute();
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Loading data...");
            progressDialog.show();
        }
        @Override
        protected String doInBackground(String... params){

            StringBuilder result = new StringBuilder();
            String data = "aaaa";
            String filename = "myprofile";


            try{
                //Connect to API
                URL url = new URL(params[0]);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setReadTimeout(10000);
                urlConnection.setConnectTimeout(10000);
                urlConnection.setRequestMethod("GET");
                urlConnection.setRequestProperty("Content-type", "application/json");
                urlConnection.connect();

                //Read API return value
                InputStream inputStream = urlConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String line;
                while((line = bufferedReader.readLine()) != null ){
                    result.append(line).append("\n");
                }
                data = result.toString();

                //Save API return value to internal storage
                FileOutputStream
                        outputStream = getActivity().openFileOutput(filename, Context.MODE_PRIVATE);
                outputStream.write(result.toString().getBytes());
                outputStream.close();
                int ch;

                //Write data from internal storage to App
                StringBuffer fileContent = new StringBuffer("");
                FileInputStream fis;
                try {
                    fis = getActivity().openFileInput( filename );
                    try {
                        while( (ch = fis.read()) != -1)
                            fileContent.append((char)ch);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    mtext.setText("Pencapaian Kosong");
                }
                data = new String(fileContent);
            }catch (IOException ex){
                int ch;
                //Write data in internal storage to app
                StringBuffer fileContent = new StringBuffer("");
                FileInputStream fis;
                try {
                    fis = getActivity().openFileInput( filename );
                    try {
                        while( (ch = fis.read()) != -1)
                            fileContent.append((char)ch);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    mtext.setText("Network Error");
                }
                data = new String(fileContent);
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result){
            String hasil;
            super.onPostExecute(result);
            try {
                JSONObject object = new JSONObject(result);
                profile = new Staff(object.getString("_id"), object.getString("kodeStaff"),
                        object.getString("namaStaff"),object.getString("kodeBranch"),
                        object.getString("namaBranch"),object.getString("kodeBMFO"));
                profileToArList=profile.getData();
                ProfileAdapter adapter = new ProfileAdapter(ctx, profileToArList);
                listView = (ListView) mView.findViewById(R.id.list_view);
                listView.setAdapter(adapter);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            nameText.setText(profile.getName());
            codeText.setText(profile.getCode());
            if(progressDialog != null){
                progressDialog.dismiss();
            }
        }
    }
}