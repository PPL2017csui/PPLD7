package com.example.hp.sibujang_app.notification;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;

import com.example.hp.sibujang_app.MainActivity;
import com.example.hp.sibujang_app.R;
import com.example.hp.sibujang_app.SessionManager;
import com.google.android.gms.gcm.GcmListenerService;

import java.util.Date;
import java.util.HashMap;

import static android.R.attr.data;

/**
 * Class representation for applying push notification on device
 */
public class NotificationsListenerService extends GcmListenerService {
    SessionManager session;
    boolean notif;

    @Override
    public void onMessageReceived(String from, Bundle data) {
        String message = data.getString("message");
        String title = data.getString("title");
        sendNotification(message, title);
        session = new SessionManager(getApplicationContext());
        //add this to check login
        notif = session.getStatusNotif();
    }

    /**
     * Send notification to the device registered
     * @param message
     * @param title
     */
    public void sendNotification(String message, String title) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        int requestCode = 0;
        PendingIntent pendingIntent = PendingIntent.getActivity(this, requestCode, intent, PendingIntent.FLAG_ONE_SHOT);
        // Setup notification
        // Sound
        Uri sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        // Build notification
        NotificationCompat.Builder notifBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent)
                .setSound(sound);
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        int id = (int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE);
        if(notif){
            notificationManager.notify(id, notifBuilder.build()); // generate unique id for multiple notifikasi
        }
    }
}
