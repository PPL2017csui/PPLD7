package com.example.hp.sibujang_app.errors;

public interface ErrorCode {
    int getCode();
}
