package com.example.hp.sibujang_app;

/**
 * Created by USER on 4/26/2017.
 */

public class Constants {
    public static final String BASE_URL = "https://sibujang-app-staging.herokuapp.com/api/authenticate";
    public static final String URL_PASSWORD = "https://sibujang-app-staging.herokuapp.com/api/password";
    public static final String TOKEN = "token";
    public static final String USERNAME = "username";
}
