package com.example.hp.sibujang_app;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by HP on 29/03/2017.
 */

public class DetailAchievementAdapter extends ArrayAdapter<String> {
    ArrayList<String> data;

    public DetailAchievementAdapter(Context context, ArrayList<String> data) {
        super(context, R.layout.detail_content, data);
        this.data=data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        String aPair = data.get(position);
        String[] content=aPair.split(":");
        // Check if an existing view is being reused, otherwise inflate the view
        convertView = LayoutInflater.from(getContext()).inflate(R.layout.detail_content, parent, false);
        // Lookup view for data population
        TextView title = (TextView) convertView.findViewById(R.id.text1);
        TextView value = (TextView) convertView.findViewById(R.id.text2);
        // Populate the data into the template view using the data object
        title.setText(content[0].substring(0,1).toUpperCase() + content[0].substring(1));
        value.setText(content[1]);
        // Return the completed view to render on screen
        return convertView;
    }
}
