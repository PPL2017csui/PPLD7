package com.example.hp.sibujang_app;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.example.hp.sibujang_app.notification.RegistrationService;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.fabric.sdk.android.Fabric;

/**
 * Created by HP on 16/05/2017.
 */

public class Manager_MainActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;
    private CustomAdapter adapter;
    private List<Achievement> data_list;
    SessionManager session;
    String kodeStaff;
    TextView mtext;
    private SearchView searchView;
    protected BroadcastReceiver mRegistrationBroadcastReceiver;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment selectedFragment = null;

            switch (item.getItemId()) {
                case R.id.navigation_pencapaian_mg:
                    getSupportActionBar().setTitle("Pencapaian Terbaru");
                    selectedFragment = ListAchievementFragment.newInstance();
                    break;
                case R.id.navigation_ringkasan_mg:
                    getSupportActionBar().setTitle("Ringkasan");
                    selectedFragment = SummaryFragment.newInstance();
                    break;
                case R.id.navigation_staff_mg:
                    getSupportActionBar().setTitle("List Staff");
                    selectedFragment = ListStaffFragment.newInstance();
                    break;
                case R.id.navigation_profil_mg:
                    getSupportActionBar().setTitle("Profil");
                    selectedFragment = ProfileFragment.newInstance();
                    break;
            }
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.frame_layout_manager, selectedFragment);
            transaction.commit();
            return true;
        }

    };

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        if (searchView != null)
            searchView =
                (SearchView) menu.findItem(R.id.search).getActionView();
        inflater.inflate(R.menu.options, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.search:
                Intent intent = new Intent(this, SearchTimLapangan.class);
                SharedPreferences.Editor editor = getSharedPreferences("myPref", MODE_PRIVATE).edit();
                editor.putString("kodeLogin", kodeStaff);
                editor.commit();
                startActivity(intent);
                return true;
            case R.id.logout:
                session.logoutUser();
                return true;
            case R.id.settings:
                Intent i = new Intent(this, PengaturanActivity.class);
                startActivity(i);
                return true;
        }
        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.GINGERBREAD)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //initialize session
        session = new SessionManager(this);
        //add this to check login
        session.checkLogin();
        // get user data from session
        HashMap<String, String> user = session.getUserDetails();

        // get string kodestaff
        kodeStaff = user.get(SessionManager.KEY_KODESTAFF);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.manager_main_activity);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Pencapaian Terbaru");
        toolbar.setTitleTextColor(0xFFFFFFFF);

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        ListAchievementFragment listAchievement= ListAchievementFragment.newInstance();
        transaction.replace(R.id.frame_layout_manager, listAchievement);
        transaction.commit();

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation_manager);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        //push notification
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                //check type of intent filter
                if(intent.getAction().endsWith(RegistrationService.REGISTRATION_SUCCESS)) {
                    // Registration success
                    String token = intent.getStringExtra("token");
                } else if (intent.getAction().equals(RegistrationService.REGISTRATION_ERROR)) {
                    // Registration error
                    Toast.makeText(getApplicationContext(), "GCM Registration error!!", Toast.LENGTH_LONG).show();
                }
            }
        };

        if (checkGooglePlayService()) {
            // Start service
            Intent intent = new Intent(this, RegistrationService.class);
            intent.putExtra("kodeStaff", kodeStaff);
            startService(intent);
        }
    }

    /**
     * Check googla play service availability
     * @return true if google play is installed, otherwise false
     */
    public boolean checkGooglePlayService() {
        // Check status google play service in device
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getApplicationContext());
        if (ConnectionResult.SUCCESS != resultCode) {
            // Check type of error
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                Toast.makeText(getApplicationContext(), "Google Play Service is not installed/enabled in this device", Toast.LENGTH_LONG).show();
                GooglePlayServicesUtil.showErrorNotification(resultCode, getApplicationContext());
            } else {
                Toast.makeText(getApplicationContext(), "This device doesn't support for Google Play Service!", Toast.LENGTH_LONG).show();
            }
            return false;
        }
        return true;
    }
}
