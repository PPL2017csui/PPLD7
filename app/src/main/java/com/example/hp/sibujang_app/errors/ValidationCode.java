package com.example.hp.sibujang_app.errors;

/**
 * Implementation of validation error code
 *
 */

public enum ValidationCode implements ErrorCode{
    VALUE_REQUIRED(201, "This field is required"),
    INVALID_FORMAT(202, "Invalid format input"),
    VALUE_TOO_SHORT(203, "Value is too short"),
    VALUE_TOO_LONGS(204, "Value is too long");

    private final int code;
    private String desc;

    private ValidationCode(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    /**
     * Gets the error validation code
     * @return the code number
     */
    @Override
    public int getCode() {
        return code;
    }

    /**
     * Get the description
     * @return the description of the error validation code
     */
    public String getDesc() {
        return desc;
    }
}
