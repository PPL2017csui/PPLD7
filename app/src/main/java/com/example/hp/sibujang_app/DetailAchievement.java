package com.example.hp.sibujang_app;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class DetailAchievement extends AppCompatActivity {
    private ListView listView;
    private String achievementTitle;
    private String achievementType;

    private ArrayList<String> detailAchievement;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_achievement);
        // Construct the data source
        achievementTitle = getIntent().getStringExtra("achievementTitle");
        detailAchievement = getIntent().getStringArrayListExtra("achievementAttribute");
        achievementType = getIntent().getStringExtra("achievementType");

        // Create the adapter to convert the array to views
        getSupportActionBar().setElevation(0);
        setTitle( Html.fromHtml("<font color='#ffffff'>Detil Pencapaian</font>"));
        DetailAchievementAdapter adapter = new DetailAchievementAdapter(this, detailAchievement);

        String[] achievementTitleSplit = achievementTitle.split("\\(");
        TextView windowTitle = (TextView) findViewById(R.id.textView5);
        windowTitle.setText(achievementTitleSplit[0]);

        TextView windowDate = (TextView) findViewById(R.id.textView6);
        windowDate.setText(achievementTitleSplit[1].substring(1,achievementTitleSplit[1].length()-1));

        TextView windowType = (TextView) findViewById(R.id.textView8);
        if(achievementType.equalsIgnoreCase("KPI")) {
            windowType.setText("Harian");
        } else {
            windowType.setText("Mingguan");
        }
        // Attach the adapter to a ListView
        ListView listView = (ListView) findViewById(R.id.achievement_list_view);
        listView.setAdapter(adapter);

    }
}
