package com.example.hp.sibujang_app;

import org.joda.time.DateTime;
import java.text.DateFormat;
import java.util.ArrayList;

/**
 * Created by HP on 29/03/2017.
 */

public class Achievement implements Comparable<Achievement>{
    private String type;
    private DateTime date;
    ArrayList<String> data;

    public Achievement(String type, String date, ArrayList<String> data) {
        this.type = type;
        this.date = new DateTime(date);
        this.data = data;
    }

    public String getType() {
        return type;
    }

    public String getDate() {
        String month = date.monthOfYear().getAsText();
        int number = date.getDayOfMonth();
        String day = date.dayOfWeek().getAsText();
        String year = date.year().getAsShortText();
        return day+", "+number+" "+month+" "+year;
    }

    public String getMonth() {
        String month = date.plusDays(1).monthOfYear().getAsShortText();
        String year = date.year().getAsShortText();
        return month+" "+year;
    }

    public ArrayList<String> getData() {
        ArrayList<String> tempData = data;
//        for(int i = 0; i < tempData.size(); i++){
//            String achievement = tempData.get(i);
//            String[] contentSplit = achievement.split(":");
//            if(contentSplit[0].equalsIgnoreCase("_id")
//                    || contentSplit[0].equalsIgnoreCase("tanggal")
//                    || contentSplit[0].equalsIgnoreCase("KodeStaff")
//                    || contentSplit[0].equalsIgnoreCase("kodeStaff")
//                    || contentSplit[0].equalsIgnoreCase("Tanggal")
//                    || contentSplit[0].equalsIgnoreCase("Jenis")
//                    || contentSplit[0].equalsIgnoreCase("Periode")
//                    || contentSplit[0].equalsIgnoreCase("periode")) {
//                tempData.remove(i);
//            }
//        }
        return tempData;
    }

    @Override
    public int compareTo(Achievement a) {

        return date.compareTo(a.date);

    }
}
