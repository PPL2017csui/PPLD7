package com.example.hp.sibujang_app;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class SearchDetailAchievement extends AppCompatActivity {
    private ListView listView;
    private String achievementTitleString;
    private ArrayList detailAchievement;
    private String namaPencapaian;
    private String tanggalPencapaian;
    private TextView achievementTitle;
    private TextView achievementDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_achievement);
        // Construct the data source
        achievementTitleString = getIntent().getStringExtra("achievementTitle");
        detailAchievement = getIntent().getStringArrayListExtra("achievementAttribute");
        namaPencapaian=getIntent().getStringExtra("namaPencapaian");
        tanggalPencapaian=getIntent().getStringExtra("achievementDate");
        //Remove _id, kodeStaf, jenis, dan tanggal pencapaian
        for(int i = 0; i < 5; i++){
            detailAchievement.remove(0);
        }

        String[] achievementSplit = achievementTitleString.split("\\(");

        // Create the adapter to convert the array to views
        achievementTitle = (TextView) findViewById(R.id.textView5);
        achievementTitle.setText(achievementSplit[0]);

        achievementDate = (TextView) findViewById(R.id.textView6);
        achievementDate.setText(tanggalPencapaian);

        TextView windowType = (TextView) findViewById(R.id.textView8);
        if(achievementSplit[0].equalsIgnoreCase("KPI")) {
            windowType.setText("Harian");
        } else {
            windowType.setText("Mingguan");
        }

        setTitle( Html.fromHtml("<font color='#ffffff'>Detail Pencapaian</font>"));
        DetailAchievementAdapter adapter = new DetailAchievementAdapter(this, detailAchievement);
        // Attach the adapter to a ListView
        ListView listView = (ListView) findViewById(R.id.achievement_list_view);
        listView.setAdapter(adapter);
//        Intent intent = new Intent();
//        intent.putExtra("namaPencapaian",namaPencapaian);
//        intent.putExtra("tanggalPencapaian",tanggalPencapaian);
//        setResult(RESULT_OK, intent);
//        finish();
    }
}
