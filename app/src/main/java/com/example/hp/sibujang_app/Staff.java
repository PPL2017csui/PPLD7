package com.example.hp.sibujang_app;

import java.util.ArrayList;

/**
 * Created by Sabiq on 05/04/2017.
 */

public class Staff{
    private String id;
    private String code;
    private String name;
    private String branchCode;
    private String branchName;
    private String BMFOCode;

    public Staff(String id, String code, String name, String branchCode, String branchName, String BMFOCode) {
        this.id = id;
        this.code = code;
        this.name = name;
        this.branchCode = branchCode;
        this.branchName = branchName;
        this.BMFOCode = BMFOCode;
    }

    public String getCode() {return code;}

    public String getName() {
        return name;
    }

    public ArrayList<String> getData() {
        ArrayList<String> data = new ArrayList<String>();
        data.add("Branch:"+this.branchName);
        data.add("Kode Branch:"+this.branchCode);
        data.add("Nama BMFO:"+this.BMFOCode);
        return data;
    }


    public String getBranchCode() {
        return branchCode;
    }

    public String getBranchName() {
        return branchName;
    }

    public String getBMFOCode() {
        return BMFOCode;
    }
}