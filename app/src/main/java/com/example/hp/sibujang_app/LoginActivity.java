package com.example.hp.sibujang_app;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import io.fabric.sdk.android.services.concurrency.AsyncTask;
import retrofit2.adapter.rxjava.HttpException;

import static com.example.hp.sibujang_app.Validation.validateFields;

public class LoginActivity extends AppCompatActivity {

    // Authentication auth;
    final Activity act = this;
    // Session Manager Class
    SessionManager session;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        // Session Manager
        session = new SessionManager(getApplicationContext());
        final EditText userName = (EditText) findViewById(R.id.userName);
        final EditText pwd = (EditText) findViewById(R.id.password);
        final Button bLogin = (Button) findViewById(R.id.bLogin);
        final TextInputLayout mTiUsername = (TextInputLayout) findViewById(R.id.ti_username);
        final TextInputLayout mTiPassword = (TextInputLayout) findViewById(R.id.ti_password);


        bLogin.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                final String kodeStaff = userName.getText().toString();
                final String[] user = userName.getText().toString().split("-");
                final String username = user[1];
                final String pass = pwd.getText().toString();
                User userTemp = new User(username,pass,kodeStaff);
                login(userTemp, mTiPassword, mTiUsername);

            }
        });
    }

    public void login(User userTemp, TextInputLayout mTiPassword, TextInputLayout mTiUsername) {

        mTiUsername.setError(null);
        mTiPassword.setError(null);


        int err = 0;

        if (!validateFields(userTemp.getName())) {

            err++;
            mTiUsername.setError("Username should not be empty !");
        }

        if (!validateFields(userTemp.getPassword())) {

            err++;
            mTiPassword.setError("Password should not be empty !");
        }

        if (err == 0) {

            loginProcess(userTemp);

        } else {
            showMessage("Enter Valid Details !");
        }


    }

    public void loginProcess(User userT) {

        PostDataTask task = new PostDataTask(act, userT);
        task.execute(Constants.BASE_URL);

    }

    public class PostDataTask extends AsyncTask<String, Void, String> {
        private Activity act;
        public User userTemp;
        ProgressDialog progressDialog;
        String result = "";
        BufferedReader bufferedReader = null;
        String errorBody;

        public PostDataTask(Activity a, User userTemp) {
            this.act = a;
            this.userTemp = userTemp;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog = new ProgressDialog(act);
            progressDialog.setMessage("Authenticating...");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {

            try {
                return postData(params[0]);
            } catch (Exception ex) {
                    return "Unknown Error";
            }
        }



        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            showMessage(result);
            if(userTemp.getToken()!=null){
                Intent intent;
                session.createLoginSession(userTemp.getName(), userTemp.getPosition(), userTemp.getKodeStaff());
               if(userTemp.getPosition()=="Penyuluh"){
                    intent = new Intent(act, MainActivity.class);
               }
               else{
                    intent = new Intent(act, Manager_MainActivity.class);
                }
                startActivity(intent);
                LoginActivity.this.finish();
            }

            if (progressDialog != null) {
                progressDialog.dismiss();
            }
        }

        private String postData(String urlPath) throws IOException, JSONException {
            //initialize and config request, then connect to server
                URL url = new URL(urlPath);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setReadTimeout(10000/*ms*/);
                urlConnection.setConnectTimeout(10000/*ms*/);
                urlConnection.setRequestMethod("POST");
                urlConnection.setDoInput(true); //enable output (body data)
                String authString = userTemp.getKodeStaff() + ":" + userTemp.getPassword();
                String authStringEnc = "Basic " + Base64.encodeToString(authString.getBytes(), Base64.NO_WRAP);
                urlConnection.setRequestProperty("Authorization", authStringEnc);
                urlConnection.connect();

                //response authentication
                if(urlConnection.getResponseCode() == 200) {
                    //read data response from server, json format
                    InputStream inputStream = urlConnection.getInputStream();
                    bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                    StringBuilder builder = new StringBuilder();
                    String line;
                    while ((line = bufferedReader.readLine()) != null) {
                        builder.append(line).append("\n");
                    }

                    bufferedReader.close();


                    result = builder.toString();

                    try {
                        //json to user
                        JSONObject object = new JSONObject(result);

                        String token = object.getString("token");
                        if (token != null) {
                            userTemp.setToken(token);
                        }
                        userTemp.setName(object.getString("message"));
                        //return response.getMessage();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else if(urlConnection.getResponseCode()==404) {
                    return "User Not Found !";
                } else if(urlConnection.getResponseCode()==401){
                    return "Invalid Password!";
                }else if(urlConnection.getResponseCode()==500){
                    return "Internal Server Error!";
                }  else {
                    return "Unknown Error";
                }

            return "Login Success!";

        }
    }


    void showMessage(String message) {
        Context ctx = getApplicationContext();
        CharSequence text = message;
        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(ctx, text, duration);
        toast.show();
    }
}
