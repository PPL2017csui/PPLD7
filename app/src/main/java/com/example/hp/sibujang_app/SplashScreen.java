package com.example.hp.sibujang_app;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;

import java.util.HashMap;

public class SplashScreen extends AppCompatActivity {
    SessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_splash_screen);


        Thread myThread = new Thread(){
            @Override
            public void run() {
                try{
                    sleep(2000);
                    session = new SessionManager(getApplicationContext());
                    HashMap<String, String> user = session.getUserDetails();
                    if(!session.isLoggedIn()){
                        session.checkLogin();
                    } else {
                        Intent intent;
                        if(user.get(SessionManager.KEY_POSITION).equals("Penyuluh")) {
                            intent = new Intent(getApplicationContext(), MainActivity.class);
                        }
                        else{
                            intent = new Intent(getApplicationContext(), Manager_MainActivity.class);
                        }
                        startActivity(intent);
                    }
                    finish();
                } catch (InterruptedException e){
                    e.printStackTrace();
                }
            }
        };
        myThread.start();
    }
}
