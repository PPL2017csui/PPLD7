package com.example.hp.sibujang_app;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;


/**
 * A simple {@link Fragment} subclass.
 */
public class SearchByLocation extends Fragment {
    private Spinner spinner;
    private Spinner spinnerName;
    private Button btnSubmit;
    private ArrayAdapter<CharSequence> adapterLocation;
    private ArrayAdapter<String> adapterName;
    private String kategori;
    private String wilayah;
    private String kodeBMFO;

    public SearchByLocation() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        SharedPreferences prefs = getActivity().getSharedPreferences("myPref", MODE_PRIVATE);
        kodeBMFO = prefs.getString("codeLocation", "No staff name defined");
        final View rootView = inflater.inflate(R.layout.fragment_search_by_location, container, false);
        spinner = (Spinner) rootView.findViewById(R.id.spinnerLocation);
        adapterLocation = ArrayAdapter.createFromResource(getActivity(), R.array.location_array, android.R.layout.simple_spinner_item);
        adapterLocation.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapterLocation);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
               kategori = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinnerName = (Spinner) rootView.findViewById(R.id.spinnerNameLocation);
        new SearchByLocation.RetrieveDB().execute("https://sibujang-app-staging.herokuapp.com/api/staffs/bmfo/BMFO-72");
        btnSubmit = (Button) rootView.findViewById(R.id.button_location);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v.getId() == btnSubmit.getId()) {
                    searchAction(rootView);
                }
            }

        });
        return rootView;
    }

    public void searchAction(View v){
        Intent intent = new Intent(getActivity(), ResultSearchTimActivity.class);
        SharedPreferences.Editor editor = getActivity().getSharedPreferences("myPref", MODE_PRIVATE).edit();
        editor.putString("wilayah", wilayah);
        editor.putString("flagWilayah", "1");
        editor.commit();
        startActivity(intent);
    }


    class RetrieveDB extends AsyncTask<String, Void, String> {

        ProgressDialog progressDialog;
        @Override
        protected void onPreExecute(){
            super.onPreExecute();
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Loading data...");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params){
            StringBuilder result = new StringBuilder();
            String data = "aaaa";
            String filename = "myfile";

            try{
                //Connect to API
                URL url = new URL(params[0]);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setReadTimeout(10000);
                urlConnection.setConnectTimeout(10000);
                urlConnection.setRequestMethod("GET");
                urlConnection.setRequestProperty("Content-type", "application/json");
                urlConnection.connect();

                //Read API return value
                InputStream inputStream = urlConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String line;
                while((line = bufferedReader.readLine()) != null ){
                    result.append(line).append("\n");
                }
                data = result.toString();

                //Save API return value to internal storage
                FileOutputStream
                        outputStream = getActivity().openFileOutput(filename, MODE_PRIVATE);
                outputStream.write(result.toString().getBytes());
                outputStream.close();
                int ch;

                //Write data from internal storage to App
                StringBuffer fileContent = new StringBuffer("");
                FileInputStream fis;
                try {
                    fis = getActivity().openFileInput( filename );
                    try {
                        while( (ch = fis.read()) != -1)
                            fileContent.append((char)ch);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                data = new String(fileContent);
            }catch (IOException ex){
            }
            //data = new String(data);
            return data;
        }


        @RequiresApi(api = Build.VERSION_CODES.CUPCAKE)
        @Override
        protected void onPostExecute(String result){
            super.onPostExecute(result);
            try {
                JSONArray myObjects = new JSONArray(result);
                ArrayList<String> dataWilayah = new ArrayList<>();
                for (int i=0; i<myObjects.length(); i++){
                    JSONObject object = myObjects.getJSONObject(i);
                    Iterator keys = object.keys();
                    while(keys.hasNext()) {
                        String attributeName= (String)keys.next();
                        String value = (object.get(attributeName)).toString();
                        if (attributeName.equals("namaBranch") && !dataWilayah.contains(value)) {
                            dataWilayah.add(value);
                        }
                    }

                }
                adapterName = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_spinner_item, dataWilayah);
                adapterName.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinnerName.setAdapter(adapterName);
                spinnerName.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        wilayah = parent.getItemAtPosition(position).toString();
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            } catch (JSONException e) {
                e.printStackTrace();
            }
            adapterName.notifyDataSetChanged();
            if(progressDialog != null){
                progressDialog.dismiss();
            }
        }
    }
}