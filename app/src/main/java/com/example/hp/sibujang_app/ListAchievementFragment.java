package com.example.hp.sibujang_app;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class ListAchievementFragment extends Fragment {
    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;
    private CustomAdapter adapter;
    private List<Achievement> data_list;
    private Context context;
    private String kodeStaffResult;
    private int totalPencapaian;
    View mView;
    SessionManager session;
    String kodeStaff;
    String position;

    public static ListAchievementFragment newInstance() {
        ListAchievementFragment fragment = new ListAchievementFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //initialize session
        session = new SessionManager(getActivity());
        //add this to check login
        session.checkLogin();
        // get user data from session
        HashMap<String, String> user = session.getUserDetails();

        // get string kodestaff
        kodeStaff = user.get(SessionManager.KEY_KODESTAFF);
        position = user.get(SessionManager.KEY_POSITION);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Bundle bundle = this.getArguments();
        String flag = "";
        if (bundle != null) {
            kodeStaffResult = bundle.getString("kodeStaff");
            flag = bundle.getString("flagSearch");
        }
        mView= inflater.inflate(R.layout.list_achievement_fragment, container, false);
        recyclerView = (RecyclerView) mView.findViewById(R.id.recycler_view_viewachievement);
        linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        context = getActivity().getApplicationContext();
        new RetrieveDB().execute("https://sibujang-app-staging.herokuapp.com/api/achievements/range/"+kodeStaff+"/2017-01-05");
        context = getActivity().getApplicationContext();
        if(position.equals("Penyuluh")){
            new RetrieveDB().execute("https://sibujang-app-staging.herokuapp.com/api/achievements/range/"+kodeStaff+"/2017-01-05");
        }
        else{
           new RetrieveDB().execute("https://sibujang-app-staging.herokuapp.com/api/achievements/range/manager/"+kodeStaff+"/2017-01-05");
        }
        if (flag.equals("1")) {
            new RetrieveDB().execute("https://sibujang-app-staging.herokuapp.com/api/achievements/"+kodeStaff);
            if (totalPencapaian == 0) {
                TextView emptySearch = (TextView) getActivity().findViewById(R.id.textView3);
            }
        }
        data_list  = new ArrayList<>();
        adapter = new CustomAdapter(getActivity(),data_list);
        recyclerView.setAdapter(adapter);
        return mView;
    }
    class RetrieveDB extends AsyncTask<String, Void, String> {

        ProgressDialog progressDialog;
        @Override
        protected void onPreExecute(){
            super.onPreExecute();
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Loading data...");
            progressDialog.show();
        }
        @Override
        protected String doInBackground(String... params){

            StringBuilder result = new StringBuilder();
            String data = "aaaa";
            String filename = "myfile";


            try{
                //Connect to API
                URL url = new URL(params[0]);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setReadTimeout(10000);
                urlConnection.setConnectTimeout(10000);
                urlConnection.setRequestMethod("GET");
                urlConnection.setRequestProperty("Content-type", "application/json");
                urlConnection.connect();

                //Read API return value
                InputStream inputStream = urlConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String line;
                while((line = bufferedReader.readLine()) != null ){
                    result.append(line).append("\n");
                }
                data = result.toString();

                //Save API return value to internal storage
                FileOutputStream
                        outputStream = getActivity().openFileOutput(filename, Context.MODE_PRIVATE);
                outputStream.write(result.toString().getBytes());
                outputStream.close();
                int ch;

                //Write data from internal storage to App
                StringBuffer fileContent = new StringBuffer("");
                FileInputStream fis;
                try {
                    fis = getActivity().openFileInput( filename );
                    try {
                        while( (ch = fis.read()) != -1)
                            fileContent.append((char)ch);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    // mtext.setText("Pencapaian Kosong");
                }
                data = new String(fileContent);
            }catch (IOException ex){
                int ch;
                //Write data in internal storage to app
                StringBuffer fileContent = new StringBuffer("");
                FileInputStream fis;
                try {
                    fis = getActivity().openFileInput( filename );
                    try {
                        while( (ch = fis.read()) != -1)
                            fileContent.append((char)ch);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    //    mtext.setText("Network Error");
                }
                data = new String(fileContent);
            }
            //data = new String(data);
            return data;
        }


        @RequiresApi(api = Build.VERSION_CODES.CUPCAKE)
        @Override
        protected void onPostExecute(String result){
            super.onPostExecute(result);
            try {
                JSONArray myObjects = new JSONArray(result);
                for (int i=0; i<myObjects.length(); i++){
                    JSONObject object = myObjects.getJSONObject(i);
                    ArrayList<String> dataAchievement= new ArrayList<>();
                    Iterator keys = object.keys();
                    while(keys.hasNext()) {
                        String attributeName= (String)keys.next();
                        if(attributeName.equalsIgnoreCase("_id")
                                || attributeName.equalsIgnoreCase("tanggal")
                                || attributeName.equalsIgnoreCase("KodeStaff")
                                || attributeName.equalsIgnoreCase("kodeStaff")
                                || attributeName.equalsIgnoreCase("Tanggal")
                                || attributeName.equalsIgnoreCase("Jenis")
                                || attributeName.equalsIgnoreCase("Periode")
                                || attributeName.equalsIgnoreCase("periode")){

                        }
                        else{
                            String value=  (object.get(attributeName)).toString();
                            dataAchievement.add(attributeName+":"+value);
                        }

                    }
                    Achievement data = new Achievement(object.getString("jenis"),object.getString("tanggal"),dataAchievement);
                    data_list.add(data);
                    Collections.sort(data_list, Collections.reverseOrder());
                }
                totalPencapaian = data_list.size();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            adapter.notifyDataSetChanged();
            if(progressDialog != null){
                progressDialog.dismiss();
            }
        }
    }
}