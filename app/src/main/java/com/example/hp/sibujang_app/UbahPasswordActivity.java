package com.example.hp.sibujang_app;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;

import io.fabric.sdk.android.services.concurrency.AsyncTask;

import static com.example.hp.sibujang_app.Validation.validateFields;

public class UbahPasswordActivity extends AppCompatActivity {
    SessionManager session;
    Activity act = this;
    String kodeStaff;
    Boolean changed;
    String temp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ubah_password);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Ubah Password");
        toolbar.setTitleTextColor(0xFFFFFFFF);
        changed = false;
        temp = "asd";

        final Button bUbahPassword = (Button) findViewById(R.id.bUbahPassword);
        final EditText oldPassword = (EditText) findViewById(R.id.oldpassword);
        final EditText newPassword = (EditText) findViewById(R.id.newpassword);
        final TextInputLayout mTiOldPassword = (TextInputLayout) findViewById(R.id.ti_oldpassword);
        final TextInputLayout mTiNewPassword = (TextInputLayout) findViewById(R.id.ti_newpassword);
        session = new SessionManager(getApplicationContext());
        //add this to check login
        session.checkLogin();
        // get user data from session
        HashMap<String, String> user = session.getUserDetails();
        // get string kodestaff
        kodeStaff = user.get(SessionManager.KEY_KODESTAFF);


        bUbahPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ubahPassword(oldPassword, newPassword, mTiOldPassword, mTiNewPassword);
            }
        });
    }
    public void ubahPassword(EditText oldPassword, EditText newPassword, TextInputLayout mTiOldPassword, TextInputLayout mTiNewPassword) {

        mTiNewPassword.setError(null);
        mTiOldPassword.setError(null);

        String oldPass = oldPassword.getText().toString();
        String newPass = newPassword.getText().toString();

        int err = 0;
        if (!validateFields(newPass)) {

            err++;
            mTiNewPassword.setError("New password should not be empty !");
        }

        if (!validateFields(oldPass)) {

            err++;
            mTiOldPassword.setError("Old password should not be empty !");
        }

        if (err == 0) {

            ubahPasswordProcess(oldPass, newPass);

        } else {
            showMessage("Enter Valid Details !");
        }


    }

    public void ubahPasswordProcess(String oldPass, String newPass) {
        PutDataTask task = new PutDataTask(oldPass, newPass);
        task.execute(Constants.URL_PASSWORD);
    }

    public class PutDataTask extends AsyncTask<String, Void, String> {
        ProgressDialog progressDialog;
        String oldPass, newPass;
        String result = "";
        BufferedReader bufferedReader = null;
        String errorBody;

        public PutDataTask (String oldPass, String newPass){
            this.oldPass = oldPass;
            this.newPass = newPass;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(act);
            progressDialog.setMessage("Loading...");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {

            try {
                return putData(params[0]);
            } catch (Exception ex) {
                return "Unknown Error";
            }
        }



        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            //showMessage(temp);
            showMessage(result);

            if(changed){
                act.finish();
            }

            if (progressDialog != null) {
                progressDialog.dismiss();
            }
        }

        private String putData(String urlPath) throws IOException, JSONException {
            //initialize and config request, then connect to server
            URL url = new URL(urlPath);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setReadTimeout(10000/*ms*/);
            urlConnection.setConnectTimeout(10000/*ms*/);
            urlConnection.setRequestMethod("POST");
            urlConnection.setDoOutput(true); //enable output (body data)
            urlConnection.setDoInput(true);
            urlConnection.setUseCaches(false);
            JSONObject jsonmsg = new JSONObject();
            jsonmsg.put("password", oldPass);
            jsonmsg.put("newPassword", newPass);
            jsonmsg.put("kodeStaff", kodeStaff);
            temp = jsonmsg.toString();
            byte[] outputBytes = temp.getBytes("UTF-8");

            urlConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            //urlConnection.connect();
            OutputStream osw = urlConnection.getOutputStream();
            osw.write(outputBytes);
            //osw.flush();
            osw.close();
            //byte[] outputBytes = json.getBytes("UTF-8");
            //String ouput = "{'kodeStaff: '"+kodeStaff+", 'password': "+oldPass+", 'newPassword': "+newPass+"}";

            //temp =urlConnection.getResponseCode()+"";
            //response authentication
            if(urlConnection.getResponseCode() == 200) {
                changed = true;
                return "Password Updated Sucessfully !";
            }else if(urlConnection.getResponseCode()==400) {
                return "Invalid Request!";
            } else if(urlConnection.getResponseCode()==401){
                return "Invalid Old Password!";
            }else if(urlConnection.getResponseCode()==500){
                return "Internal Server Error!";
            }  else {
                return "Unknown Error";
            }
        }
    }


    void showMessage(String message) {

        Context context = getApplicationContext();
        CharSequence text = message;
        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(context, text, duration);
        toast.show();
    }
}
