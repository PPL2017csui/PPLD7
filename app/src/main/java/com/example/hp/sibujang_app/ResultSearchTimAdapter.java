package com.example.hp.sibujang_app;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;
/**
 * Created by HP on 29/03/2017.
 */

public class ResultSearchTimAdapter extends RecyclerView.Adapter<ResultSearchTimAdapter.ViewHolder> {

    private Context context;
    private List<Achievement> my_data;
    private String namaPencapaian;
    private String tanggalPencapaian;

    public ResultSearchTimAdapter(Context context, List<Achievement> my_data) {
        this.context = context;
        this.my_data = my_data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card,parent,false);

        return new ViewHolder(itemView,this.context,this.my_data);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.achievement_date.setText( my_data.get(position).getDate());
        holder.achievement_type.setText(my_data.get(position).getType());


    }


    @Override
    public int getItemCount() {
        return my_data.size();
    }
    public Context getContext(){
        return context;
    }




    public  class ViewHolder extends  RecyclerView.ViewHolder implements View.OnClickListener{

        public TextView achievement_date;
        public TextView achievement_type;
        private String achievementTitle;
        private Context context;
        private List<Achievement> my_data;

        public ViewHolder(View itemView, Context context, List<Achievement> my_data) {
            super(itemView);
            this.my_data=my_data;
            this.context=context;
            achievement_date = (TextView) itemView.findViewById(R.id.Achievement_date);
            achievement_type = (TextView) itemView.findViewById(R.id.Achievement_type);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position= getAdapterPosition();
            Achievement objectAchievement = this.my_data.get(position);
            achievementTitle=objectAchievement.getType() + "( " +  objectAchievement.getDate() + ")";
            Intent intent = new Intent(this.context, SearchDetailTim.class);
            intent.putStringArrayListExtra("achievementAttribute",(ArrayList<String>) objectAchievement.getData());
            intent.putExtra("achievementTitle",achievementTitle);
            context.startActivity(intent);
        }
    }
}
