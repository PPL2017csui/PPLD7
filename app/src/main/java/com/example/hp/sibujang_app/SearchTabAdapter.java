package com.example.hp.sibujang_app;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

//import com.bumptech.glide.Glide;

import org.w3c.dom.Text;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by HP on 29/03/2017.
 */

public class SearchTabAdapter extends RecyclerView.Adapter<SearchTabAdapter.ViewHolder> {

    private Context context;
    private List<Staff> my_data;
    private String staffName;

    public SearchTabAdapter(Context context, List<Staff> my_data) {
        this.context = context;
        this.my_data = my_data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_staff_card,parent,false);
        return new ViewHolder(itemView,this.context,this.my_data);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.code.setText(my_data.get(position).getCode());
        holder.name.setText(my_data.get(position).getName());
    }


    @Override
    public int getItemCount() {
        return my_data.size();
    }
    public Context getContext(){
        return context;
    }




    public  class ViewHolder extends  RecyclerView.ViewHolder implements View.OnClickListener{

//        public TextView achievement_date;
        public TextView name;
        public TextView code;
        private String kodeStaff;
        private Context context;
        private List<Staff> my_data;

        public ViewHolder(View itemView, Context context, List<Staff> my_data) {
            super(itemView);
            this.my_data=my_data;
            this.context=context;
            code = (TextView) itemView.findViewById(R.id.staff_code);
            name = (TextView) itemView.findViewById(R.id.staff_name);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position= getAdapterPosition();
            Staff objectStaff = this.my_data.get(position);
            staffName = objectStaff.getName();
            String staffCode = objectStaff.getCode();
            Intent intent = new Intent(this.context, ResultSearchAchievementActivity.class);
            SharedPreferences.Editor editor = this.context.getSharedPreferences("myPref", MODE_PRIVATE).edit();
            editor.putString("staffCode", staffCode);
            editor.commit();
            this.context.startActivity(intent);
        }
    }
}
