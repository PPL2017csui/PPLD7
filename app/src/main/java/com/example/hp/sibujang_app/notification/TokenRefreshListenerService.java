package com.example.hp.sibujang_app.notification;

import android.content.Intent;
import com.google.android.gms.iid.InstanceIDListenerService;

/**
 * Class representation for get another registration token GCM
 */

public class TokenRefreshListenerService extends InstanceIDListenerService {
    @Override
    public void onTokenRefresh() {
        Intent i = new Intent(this, RegistrationService.class);
        startService(i);
    }
}
