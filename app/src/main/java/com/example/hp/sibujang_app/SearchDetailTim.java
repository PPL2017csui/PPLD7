package com.example.hp.sibujang_app;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.widget.ListView;
import java.util.ArrayList;

public class SearchDetailTim extends AppCompatActivity {
    private ListView listView;
    private String achievementTitle;
    private ArrayList detailAchievement;
    private String namaPencapaian;
    private String tanggalPencapaian;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result_detail_tim_ach);
        // Construct the data source
        achievementTitle = getIntent().getStringExtra("achievementTitle");
        detailAchievement = getIntent().getStringArrayListExtra("achievementAttribute");
        namaPencapaian=getIntent().getStringExtra("namaPencapaian");
        tanggalPencapaian=getIntent().getStringExtra("tanggalPencapaian");
        //Remove _id, kodeStaf, jenis, dan tanggal pencapaian
        for(int i = 0; i < 5; i++){
            detailAchievement.remove(0);
        }
        // Create the adapter to convert the array to views

        setTitle( Html.fromHtml("<font color='#ffffff'>Detail Pencapaian</font>"));
        DetailAchievementAdapter adapter = new DetailAchievementAdapter(this, detailAchievement);
        // Attach the adapter to a ListView
        ListView listView = (ListView) findViewById(R.id.achievement_list_view);
        listView.setAdapter(adapter);
//        Intent intent = new Intent();
//        intent.putExtra("namaPencapaian",namaPencapaian);
//        intent.putExtra("tanggalPencapaian",tanggalPencapaian);
//        setResult(RESULT_OK, intent);
//        finish();
    }
}
