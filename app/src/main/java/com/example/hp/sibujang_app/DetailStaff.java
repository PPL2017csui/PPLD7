package com.example.hp.sibujang_app;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class DetailStaff extends AppCompatActivity {
    private ListView listView;
    private ArrayList detailStaff;
    private String staffName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_staff);
        // Construct the data source
        staffName = getIntent().getStringExtra("staffName");
        detailStaff = getIntent().getStringArrayListExtra("staffData");

        // Create the adapter to convert the array to views
        setTitle( Html.fromHtml("<font color='#ffffff'>Detail Staff</font>"));
        DetailStaffAdapter adapter = new DetailStaffAdapter(this, detailStaff);

        TextView staffNameView = (TextView) findViewById(R.id.staff_name_textview);
        staffNameView.setText(staffName);

        // Attach the adapter to a ListView
        ListView listView = (ListView) findViewById(R.id.staff_list_view);
        listView.setAdapter(adapter);
    }
}
