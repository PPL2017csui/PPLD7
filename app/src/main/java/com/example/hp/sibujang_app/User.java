package com.example.hp.sibujang_app;


/**
 * Created by Rizky Noviandi Purwono on 4/26/2017.
 */

public class User {

    private String name;
    private String password;
    private String position;
    //private String created_at;
    private String kodeStaff;
    private String token;

    public User(String name, String password, String kodeStaff){
        setName(name);
        setPassword(password);
        setKodeStaff(kodeStaff);
        setPosition(kodeStaff);
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setKodeStaff(String kodeStaff) {
        this.kodeStaff = kodeStaff;
    }

    public String getPassword() {
        return password;
    }

    public String getKodeStaff() { return kodeStaff; }

    public String getName() {
        return name;
    }

    public void setPosition(String position){
        String[] temp = position.split("-");
        if(temp[0].equals("000")){
            this.position = "Penyuluh";
        } else {
            this.position = "Manager";
        }
    }

    public String getPosition(){
        return position;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getToken(){
        return token;
    }

}
