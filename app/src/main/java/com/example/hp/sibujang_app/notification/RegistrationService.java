package com.example.hp.sibujang_app.notification;

import com.example.hp.sibujang_app.errors.HttpStatusCode;
import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;
import com.example.hp.sibujang_app.MainActivity;
import com.example.hp.sibujang_app.R;
import com.example.hp.sibujang_app.User;
import com.google.android.gms.gcm.GcmPubSub;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.IllegalFormatException;
import java.util.Iterator;
import java.util.Map;

/**
 * Class representation for doing registration device to Google Cloud Messaging
 */

public class RegistrationService extends IntentService {
    public static final String REGISTRATION_SUCCESS = "RegistrationSuccess";
    public static final String REGISTRATION_ERROR = "RegistrationError";
    public static final String TAG = "GCMTOKEN";
    private String kodeStaff;

    public RegistrationService() {
        super("RegistrationService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        kodeStaff = intent.getStringExtra("kodeStaff");
        registerGCM();
    }

    /**
     * Gets registration token device from GCM and save it to SharedPref
     */
    public void registerGCM() {
        SharedPreferences sharedPreferences = getSharedPreferences("GCM", Context.MODE_PRIVATE); //Define shared reference file name
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Intent registrationComplete = null;
        String token = null;
        try {
            InstanceID instanceID = InstanceID.getInstance(getApplicationContext());
            token = instanceID.getToken(getString(R.string.gcm_defaultSenderId), GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
            Log.w("RegService", "token: " + token);
            // notify to UI registration complete success
            registrationComplete = new Intent(REGISTRATION_SUCCESS);
            registrationComplete.putExtra("token", token);
            String oldToken = sharedPreferences.getString(TAG, ""); // Return "" when error or key not exists
            if (!"".equals(token) && !oldToken.equals(token)) {
                saveTokenToServer(token);
                // Sabe new token to shared reference
                editor.putString("GCMTOKEN", token);
                editor.commit();
            } else {
                Log.w("RegistrationService", "Old Token");
            }
        } catch (Exception e) {
            Log.w("RegService", "Registration error");
            registrationComplete = new Intent(REGISTRATION_ERROR);
        }
        //send broadcast
        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
    }

    /**
     * Setup parameter to access API Sibujang
     * @param token
     */
    public void saveTokenToServer(String token) {
        Map paramPost = new HashMap();
        paramPost.put("registrationId", token);
        paramPost.put("kodeStaff", kodeStaff);
        try {
            String msgResult = getStringResultFromService_POST("https://sibujang-app-staging.herokuapp.com/devices", paramPost);
            Log.w("ServiceResponseMsg", msgResult);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Connect to API Sibujang and post the device
     * @param serviceURL
     * @param params
     * @return response body as result
     */
    public String getStringResultFromService_POST(String serviceURL, Map<String, String> params) {
        String resultString = null;
        HttpURLConnection connection = null;
        String line = null;
        URL url;
        try {
            url = new URL(serviceURL);
        } catch (MalformedURLException e) {
            throw new IllegalArgumentException("URL invalid: " + serviceURL);
        }
        StringBuilder bodyBuilder = new StringBuilder();
        Iterator<Map.Entry<String, String>> iterator = params.entrySet().iterator();
        // Construct post body using parameter
        while (iterator.hasNext()) {
            Map.Entry<String, String> param = iterator.next();
            bodyBuilder.append(param.getKey()).append('=').append(param.getValue());
            if (iterator.hasNext()) {
                bodyBuilder.append('&');
            }
            String body = bodyBuilder.toString(); // format same to arg1=val1&arg2=val2
            Log.w("AccessService", "param: " + body);
            byte [] bytes = body.getBytes();
            try {
                connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.setDoOutput(true);
                connection.setUseCaches(false);
                connection.setFixedLengthStreamingMode(bytes.length);
                connection.setRequestMethod("POST");
                connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                // Post the request
                OutputStream output = connection.getOutputStream();
                output.write(bytes);
                output.close();
                // Handle the response
                int status = connection.getResponseCode();
                Log.w("status", ""+status+"");
                if (status != 200) {
                    for (HttpStatusCode stat : HttpStatusCode.values()) {
                        if (status == stat.getCode()) {
                            throw new IOException(stat.getDesc());
                        }
                    }
                }
                BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                StringBuilder stringBuilder = new StringBuilder();
                while ((line = reader.readLine()) != null) {
                    stringBuilder.append(line+"\n");
                }
                resultString = stringBuilder.toString();
            } catch (Exception e) {
                e.printStackTrace();
                resultString = null;
            }
        }
        System.out.println(resultString);
        Log.w("hasil", resultString);
        return resultString;
    }
}
